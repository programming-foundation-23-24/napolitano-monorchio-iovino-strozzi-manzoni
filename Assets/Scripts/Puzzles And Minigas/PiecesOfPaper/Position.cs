using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position : MonoBehaviour
{
    public int spotNumber;

    public int GetSpotNumber()
    {
        return spotNumber;
    }
}
