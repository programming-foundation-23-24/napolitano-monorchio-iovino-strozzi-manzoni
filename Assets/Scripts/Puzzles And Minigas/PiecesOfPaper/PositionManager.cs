using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PositionManager : MonoBehaviour
{
    
    public GameObject[] positionsObjects;
    public List<Transform> positions = new List<Transform>();
    public GameObject[] piecesOfPaperObjects;
    public List<PieceOfPaper> piecesOfPaper = new List<PieceOfPaper>();
    public UnityEvent OnWin;
    public int PosizioniCorrette = 0;

    private void Start()
    {
        piecesOfPaperObjects = GameObject.FindGameObjectsWithTag("PieceOfPaper");
        positionsObjects = GameObject.FindGameObjectsWithTag("Spot");
        foreach (GameObject spot in positionsObjects)
        {
            positions.Add(spot.transform);
        }
        if (positions.Count == 0)
        {
            Debug.LogError("Non ci sono posizioni nel manager");
        }
        foreach (GameObject piece in piecesOfPaperObjects)
        {
            piecesOfPaper.Add(piece.GetComponent<PieceOfPaper>());
        }

    }

    public List<Transform> GetPositions()
    {
        return positions;
    }

    private void Update()
    {
        CheckCompletion();
    }

    void CheckCompletion()
    {
        int correctCount = 0;
        foreach (PieceOfPaper piece in piecesOfPaper)
        {
            if (piece.CorrectPosition)
            {
                correctCount++;
            }
        }

        if (correctCount == piecesOfPaper.Count)
        {
            Debug.Log("Minigioco completato");
            OnWin.Invoke();
        }
        else
        {
            Debug.Log("Minigioco non completato");
        }
    }
}
