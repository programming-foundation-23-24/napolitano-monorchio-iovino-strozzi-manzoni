using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ManagerDeiPezziDiCarta : MonoBehaviour
{
    public UnityEvent onWin;
    public List<PezzoDiCarta> pieces = new List<PezzoDiCarta>();

    void Start()
    {
        Transform parentTransform = transform;
        foreach (Transform childTransform in parentTransform)
        {
            PostoPerPezzoDiCarta spotComponent = childTransform.GetComponent<PostoPerPezzoDiCarta>();
            if (spotComponent != null)
            {
                PezzoDiCarta pieceComponent = childTransform.GetComponentInChildren<PezzoDiCarta>();
                if (pieceComponent != null)
                {
                    pieces.Add(pieceComponent);
                }
            }
        }
    }

    void Update()
    {
        bool allCorrectPlaces = true;

        foreach (var item in pieces)
        {
            if (!item.isCorrectPlace)
            {
                allCorrectPlaces = false;
                break;
            }
        }

        if (allCorrectPlaces)
        {
            onWin.Invoke();
        }
    }
}
