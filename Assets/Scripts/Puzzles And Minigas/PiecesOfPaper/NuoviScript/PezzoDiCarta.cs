using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PezzoDiCarta : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private RectTransform rectTransform;
    private Canvas canvas;
    private CanvasGroup canvasGroup;
    private Vector2 originalPosition; // Memorizza la posizione originale dell'oggetto
    private Transform originalParent; // Memorizza il genitore originale dell'oggetto
    private Transform draggyDaddy;
    public int numeroPezzoCarta;
    public bool isCorrectPlace = false;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvas = GetComponentInParent<Canvas>();
        canvasGroup = GetComponent<CanvasGroup>();
        originalPosition = rectTransform.anchoredPosition; // Memorizza la posizione originale dell'oggetto al momento dell'Awake
        originalParent = transform.parent; // Memorizza il genitore originale dell'oggetto al momento dell'Awake
    }
    private void Start()
    {
        draggyDaddy = GameObject.FindGameObjectWithTag("DragPieceParent").transform;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        // Rimuove il genitore attuale dell'oggetto
        originalParent = transform.parent;
        transform.SetParent(null);

        canvasGroup.alpha = 0.6f;
        canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
        rectTransform.SetParent(draggyDaddy, true);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.alpha = 1f;
        canvasGroup.blocksRaycasts = true;

        // Check if the dragged object is touching an image tagged as "Spot"
        GameObject parent = GetRaycastParent(eventData);
        if (parent != null)
        {
            // If the parent already has a child, make the dragged object the new child
            if (parent.transform.childCount > 0)
            {
                Transform currentChild = parent.transform.GetChild(0);
                currentChild.SetParent(originalParent, false);
                currentChild.GetComponent<RectTransform>().anchoredPosition = originalPosition;
            }

            // Set the dragged object's parent to the parent and position it correctly
            rectTransform.SetParent(parent.transform, false);
            rectTransform.anchoredPosition = Vector2.zero;

            // Controlla se il numero dello spot � uguale al numero del pezzo di carta
            PostoPerPezzoDiCarta posto = parent.GetComponent<PostoPerPezzoDiCarta>();
            if (posto != null && posto.GetSpotNumber() == numeroPezzoCarta - 1)
            {
                isCorrectPlace = true;
            }
            else
            {
                isCorrectPlace = false;
            }
        }
        else
        {
            // Se l'oggetto non � stato rilasciato su uno spot, ritorna alla sua posizione originale
            rectTransform.SetParent(originalParent, false);
            rectTransform.anchoredPosition = originalPosition;
        }
    }

    public bool GetIsCorrect()
    {
        return isCorrectPlace;
    }
    private void Update()
    {
      
            GameObject parent = rectTransform.parent.gameObject;
            PostoPerPezzoDiCarta posto = parent.GetComponent<PostoPerPezzoDiCarta>();
            if (posto != null && posto.GetSpotNumber() == numeroPezzoCarta)
            {
                isCorrectPlace = true;
            }
            else
            {
                isCorrectPlace = false;
            }
        
     
    }

    private GameObject GetRaycastParent(PointerEventData eventData)
    {
        var raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raycastResults);
        foreach (var result in raycastResults)
        {
            // Verifica se l'oggetto rilevato � uno "Spot"
            if (result.gameObject.CompareTag("Spot"))
            {
                return result.gameObject;
            }
        }
        return null;
    }

}
