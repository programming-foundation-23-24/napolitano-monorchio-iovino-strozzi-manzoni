using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceOfPaper : MonoBehaviour
{
    private bool isBeingDragged = false;
    private Vector2 initialPosition;
    private Vector2 mouseOffset;
    private PositionManager positionManager;
    private List<Transform> positions = new List<Transform>();
    public int positionNumber;
    public bool CorrectPosition;

    private void Start()
    {
        positionManager = FindObjectOfType<PositionManager>(); // Usa FindObjectOfType invece di FindAnyObjectByType
        if (!positionManager)
        {
            Debug.LogError("Non trovo il position manager");
        }
        positions = positionManager.GetPositions();
        if (positions == null)
        {
            Debug.LogError("Non riesco a prendere la lista delle posizioni");
        }
    }

    void OnMouseDown()
    {
        isBeingDragged = true;
        initialPosition = transform.position;
        mouseOffset = (Vector2)transform.position - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    void OnMouseDrag()
    {
        if (isBeingDragged)
        {
            Vector2 currentMousePosition = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition) + mouseOffset;
            // Aggiorna la posizione del pezzo di carta in base alla posizione del mouse
            transform.position = currentMousePosition;
        }
    }

    void OnMouseUp()
    {
        isBeingDragged = false;

        // Controlla se il pezzo di carta ha un genitore
        if (transform.parent != null)
        {
            // Trova la posizione pi� vicina e muovi il pezzo di carta
            float closestDistance = float.MaxValue;
            Transform closestPosition = null;

            int currentSpotNumber = -1; // Impostiamo il valore di default a -1

            // Controlliamo se il pezzo di carta ha un componente Position nel genitore
            Position currentPositionComponent = transform.parent.GetComponent<Position>();
            if (currentPositionComponent != null)
            {
                currentSpotNumber = currentPositionComponent.GetSpotNumber();
            }

            foreach (Transform pos in positions)
            {
                int spotNumber = pos.GetComponent<Position>().GetSpotNumber();
                if (spotNumber != currentSpotNumber)
                {
                    float distance = Vector2.Distance(transform.position, pos.position);
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestPosition = pos;
                    }
                }
            }

            if (closestPosition != null)
            {
                // Controlla se la posizione pi� vicina ha gi� un figlio
                PieceOfPaper existingPiece = closestPosition.GetComponentInChildren<PieceOfPaper>();
                if (existingPiece != null)
                {
                    // Memorizza la posizione corrente dell'altro pezzo di carta
                    Vector2 existingPieceCurrentPosition = existingPiece.initialPosition;

                    // Trasferisci l'altro pezzo di carta nella posizione iniziale del pezzo corrente
                    existingPiece.transform.SetParent(transform.parent);
                    existingPiece.transform.position = initialPosition;
                }

                // Sposta il pezzo di carta corrente nella posizione del pezzo di carta vicino
                transform.SetParent(closestPosition);
                transform.position = closestPosition.position;
            }
            else
            {
                // Se non ci sono posizioni libere, riporta il pezzo di carta alla posizione iniziale
                ResetPosition();
            }
        }
        else
        {
            Debug.LogWarning("Il pezzo di carta non ha un genitore.");
        }
    }

    public void ResetPosition()
    {
        transform.position = initialPosition;
    }

    void Update()
    {
        // Controlla se il positionNumber � corretto
        if (positionNumber == GetCurrentPositionNumber())
        {
            CorrectPosition = true;
        }
        else
        {
            CorrectPosition = false;
        }
    }

    int GetCurrentPositionNumber()
    {
        // Ottieni il numero di posizione corrente del pezzo di carta
        if (transform.parent != null)
        {
            Position currentPositionComponent = transform.parent.GetComponent<Position>();
            if (currentPositionComponent != null)
            {
                return currentPositionComponent.GetSpotNumber();
            }
        }

        // Se il pezzo di carta non ha un genitore o il componente Position, restituisci -1
        return -1;
    }

    public bool GetCorrectPosition()
    {
        return CorrectPosition;
    }
}
