using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{
    public PuzzleManager puzzleManager;

    private Vector3 initialPosition;
    private bool isPlacedCorrectly = false;

    private void Start()
    {
        initialPosition = transform.position;
        if (puzzleManager == null)
        {
            puzzleManager = FindObjectOfType<PuzzleManager>();
        }
    }

    private void OnMouseDown()
    {
        if (!isPlacedCorrectly)
        {
            puzzleManager.StartDragging(transform);
        }
    }

    private void OnMouseDrag()
    {
        if (!isPlacedCorrectly && puzzleManager)
        {
            transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));
        }
    }

    private void OnMouseUp()
    {
        if (!isPlacedCorrectly && puzzleManager)
        {
            puzzleManager.EndDragging();

            bool pieceOverlapping = false;
            PuzzlePiece overlappingPiece = null;

            foreach (Transform baseTransform in puzzleManager.bases)
            {
                if (Vector3.Distance(transform.position, baseTransform.position) < 0.5f)
                {
                    foreach (Transform piece in puzzleManager.puzzlePieces)
                    {
                        if (piece != transform && Vector3.Distance(transform.position, piece.position) < 0.5f)
                        {
                            pieceOverlapping = true;
                            overlappingPiece = piece.GetComponent<PuzzlePiece>();
                            break;
                        }
                    }
                    if (pieceOverlapping)
                        break;

                    puzzleManager.MovePieceToBase(baseTransform);
                    return;
                }
            }

            if (pieceOverlapping && overlappingPiece != null)
            {
                Vector3 tempPosition = overlappingPiece.transform.position;
                overlappingPiece.transform.position = transform.position;
                transform.position = tempPosition;
                puzzleManager.CheckPuzzleCompletion(); // Aggiunto per controllare se il puzzle è stato risolto dopo lo scambio di posizione.
            }
            else
            {
                transform.position = initialPosition;
            }
        }
    }

    public void SetCorrectPosition(Vector3 position)
    {
        isPlacedCorrectly = true;
        transform.position = position;
    }

    public void ResetPosition()
    {
        isPlacedCorrectly = false;
        transform.position = initialPosition;
    }

    public bool IsPlacedCorrectly()
    {
        return isPlacedCorrectly;
    }
}


