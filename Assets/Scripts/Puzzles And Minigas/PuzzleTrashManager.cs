using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : MonoBehaviour
{
    public Transform[] puzzlePieces;
    public Transform[] bases;
    private bool puzzleCompleted = false;

    private bool isDragging = false;
    private Transform draggedPiece;
    private Vector3 initialPiecePosition;

    private void Update()
    {
        if (isDragging && Input.GetMouseButtonUp(0))
        {
            isDragging = false;
            CheckPuzzleCompletion();
        }
    }

    public void CheckPuzzleCompletion()
    {
        foreach (Transform baseTransform in bases)
        {
            bool pieceFound = false;
            foreach (Transform piece in puzzlePieces)
            {
                if (Vector3.Distance(piece.position, baseTransform.position) < 0.5f)
                {
                    pieceFound = true;
                    break;
                }
            }
            if (!pieceFound)
            {
                puzzleCompleted = false;
                return;
            }
        }
        puzzleCompleted = true;
        Debug.Log("Puzzle completed!");
    }

    public void StartDragging(Transform piece)
    {
        isDragging = true;
        draggedPiece = piece;
        initialPiecePosition = piece.position;
    }

    public void EndDragging()
    {
        isDragging = false;
        draggedPiece = null;
    }

    public void MovePieceToBase(Transform baseTransform)
    {
        foreach (Transform piece in puzzlePieces)
        {
            if (Vector3.Distance(piece.position, baseTransform.position) < 0.5f)
            {
                if (piece != draggedPiece)
                {
                    // Scambia le posizioni tra il pezzo trascinato e quello già presente sulla base.
                    Vector3 temp = piece.position;
                    piece.position = initialPiecePosition;
                    initialPiecePosition = temp;
                    break;
                }
            }
        }
        draggedPiece.position = baseTransform.position;
        CheckPuzzleCompletion();
    }

    public bool IsPuzzleCompleted()
    {
        return puzzleCompleted;
    }
}
