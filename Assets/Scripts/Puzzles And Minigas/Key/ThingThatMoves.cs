using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThingThatMoves : MonoBehaviour, IPointerUpHandler
{
    public int CorrectYposition;
    public bool isCorrectPosition;
    public int[] yPosition = new int[3];
    public Sprite[] images = new Sprite[3];
    Image image;
    public int position = 0;

    RectTransform rectTransform;

    void Start()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
        image.sprite = images[position];
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        position++;
        if (position == 3) position = 0;
        image.sprite = images[position]; 
    }

    public int GetPosition()
    {
        return yPosition[position];
    }

    void Update()
    {
        if (position == CorrectYposition)
        {
            isCorrectPosition = true;
        }
        else
        {
            isCorrectPosition = false;
        }
    }

    public bool GetIsCorrect()
    {
        return isCorrectPosition;
    }
}
