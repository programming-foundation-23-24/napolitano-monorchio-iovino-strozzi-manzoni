using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class KeyManager : MonoBehaviour, IPointerUpHandler
{
    private GameObject[] arrayOfThingsThatMove;
    public List<ThingThatMoves> thingsThatMove = new List<ThingThatMoves>();
    public List<bool> Lights = new List<bool>();
    private Image image;
    public UnityEvent OnWin;

    void Start()
    {
        AutoPopulateLists();
        image = gameObject.GetComponent<Image>();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        CheckMinigameResult();
    }

    void AutoPopulateLists()
    {
        arrayOfThingsThatMove = GameObject.FindGameObjectsWithTag("ThingThatMoves");
        foreach (GameObject item in arrayOfThingsThatMove)
        {
            thingsThatMove.Add(item.GetComponent<ThingThatMoves>());
        }
    }

    void CheckMinigameResult()
    {
        foreach (var item in thingsThatMove)
        {
            if (OneItemIsWrong(item))
            {
                Fail();
                return; // Termina subito il metodo se si trova un errore
            }
        }
        Win();
    }

    bool OneItemIsWrong(ThingThatMoves item)
    {
        return !item.GetIsCorrect();
    }

    void Win()
    {
        Debug.Log("Minigioco Completato!");
        image.color = Color.green;
        OnWin.Invoke();
    }

    void Fail()
    {
        image.color = Color.red;
       
    }
}
