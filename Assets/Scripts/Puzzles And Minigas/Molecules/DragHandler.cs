using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class DragHandler : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private RectTransform _rectTransform;
    private Canvas _canvas;
    private CanvasGroup _canvasGroup;
    public char molecule;
    public bool isCorrectPlace;

    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _canvas = GetComponentInParent<Canvas>();
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        isCorrectPlace = false;
        if (_canvas.renderMode == RenderMode.ScreenSpaceOverlay || _canvas.renderMode == RenderMode.WorldSpace)
        {
            _rectTransform.anchoredPosition += eventData.delta / _canvas.scaleFactor;
        }
        else
        {
            _rectTransform.anchoredPosition += eventData.delta;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _canvasGroup.blocksRaycasts = true;

        PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        pointerEventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, results);

        foreach (RaycastResult result in results)
        {
            if (result.gameObject.CompareTag("Spot"))
            {
                MoleculesSpot moleculesSpot = result.gameObject.GetComponent<MoleculesSpot>();
                if (moleculesSpot != null && moleculesSpot.moleculeSpot == molecule)
                {
                    Debug.Log("Giusto");
                    isCorrectPlace = true;
                }
                else
                {
                    Debug.Log("Sbagliato");
                    isCorrectPlace = false;
                }
                _rectTransform.position = result.gameObject.transform.position;
                break;
            }
        }
    }
    public bool GetIsCorrectPlace()
    {
        return isCorrectPlace;
    }
}
