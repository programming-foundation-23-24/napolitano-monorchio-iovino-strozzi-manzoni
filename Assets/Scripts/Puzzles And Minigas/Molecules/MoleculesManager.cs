using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MoleculesManager : MonoBehaviour
{
    private GameObject[] arrayOfmolecules;
    public UnityEvent onWin;
    public List<DragHandler> molecules = new List<DragHandler>();
    private void Start()
    {
        arrayOfmolecules = GameObject.FindGameObjectsWithTag("Molecule");
        foreach (GameObject item in arrayOfmolecules)
        {
            molecules.Add(item.GetComponent<DragHandler>());
        }

    }
    private void Update()
    {
        CheckMinigameResult();
    }
    void CheckMinigameResult()
    {
        foreach (var item in molecules)
        {
            if (OneItemIsWrong(item))
            {
                Fail();
                return; 
            }
        }
        Win();
    }

    bool OneItemIsWrong(DragHandler item)
    {
        return !item.GetIsCorrectPlace();
    }

    void Win()
    {
        Debug.Log("Minigioco Completato!");
        onWin.Invoke();
        
    }

    void Fail()
    {
        Debug.Log("Non completato");
    }
}
