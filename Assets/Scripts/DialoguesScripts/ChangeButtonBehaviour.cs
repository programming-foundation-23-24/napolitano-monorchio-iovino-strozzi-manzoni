using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeButtonBehaviour : MonoBehaviour
{
    public UnityEvent OnBoolTrue;
    public UnityEvent OnBoolFalse;
    public bool changeBehaviour = false;

    public void ChangeBehaviourCondition()
    {
        changeBehaviour = !changeBehaviour;
    }

    public void ChangeBehaviour()
    {
        if (changeBehaviour)
        {
            OnBoolTrue.Invoke();
        }
        else
        {
            OnBoolFalse.Invoke();
        }
    }
}
