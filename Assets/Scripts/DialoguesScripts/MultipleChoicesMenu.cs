using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultipleChoicesMenu : LayoutGroup
{
    public float spacing; // Spaziatura tra gli elementi
    public Vector2 startOffset; // Offset di partenza per il primo elemento
    public bool[] childActiveStates;

    protected override void OnEnable()
    {
        base.OnEnable();
        CalculateLinear();
    }

    public override void CalculateLayoutInputVertical()
    {
        CalculateLinear();
    }

    public override void CalculateLayoutInputHorizontal()
    {
        CalculateLinear();
    }

    void LateUpdate()
    {
        CalculateLinear();
    }
    public override void SetLayoutHorizontal()
    {
    }

    public override void SetLayoutVertical()
    {
    }

    void CalculateLinear()
    {
        m_Tracker.Clear();

        // Calcola il numero totale di figli attivi
        int activeChildCount = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            bool isActive = i < childActiveStates.Length ? childActiveStates[i] : true;
            if (isActive)
            {
                activeChildCount++;
            }
        }

        // Calcola la posizione Y iniziale
        float yPosition = startOffset.y - (spacing * (activeChildCount - 1) / 2);

        int activeChildIndex = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            RectTransform child = (RectTransform)transform.GetChild(i);

            if (child != null)
            {
                bool isActive = i < childActiveStates.Length ? childActiveStates[i] : true;
                if (isActive)
                {
                    m_Tracker.Add(this, child, DrivenTransformProperties.Anchors | DrivenTransformProperties.AnchoredPosition | DrivenTransformProperties.Pivot);

                    Vector3 vPos = new Vector3(startOffset.x, yPosition + spacing * activeChildIndex, 0);
                    child.localPosition = vPos;

                    child.anchorMin = child.anchorMax = child.pivot = new Vector2(0.5f, 0.5f);
                    child.gameObject.SetActive(true);

                    activeChildIndex++;
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
    }
}