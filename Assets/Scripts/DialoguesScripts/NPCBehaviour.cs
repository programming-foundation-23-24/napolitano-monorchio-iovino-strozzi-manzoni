using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class NPCBehaviour : MonoBehaviour
{
    [SerializeField]
    public Transform[] destinationPoints;

    public float moveSpeed = 5.0f;
    public UnityEvent OnNPCBehaviourStart;
    public UnityEvent OnNPCBehaviourOver;

    private int currentDestinationIndex = 0;

    public void NPCMovementInizialization()
    {
        if (destinationPoints.Length == 0)
        {
            Debug.LogError("Aggiungi almeno un punto di destinazione all'array.");
            return;
        }
        OnNPCBehaviourStart.Invoke();
        StartCoroutine(MoveToNextDestination());
    }

    IEnumerator MoveToNextDestination()
    {
        while (currentDestinationIndex < destinationPoints.Length)
        {
            Transform target = destinationPoints[currentDestinationIndex];

            while (Vector2.Distance(transform.position, target.position) > 0.01f)
            {
                transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
                yield return null;
            }

            currentDestinationIndex++;

            yield return new WaitForSeconds(0.1f);
        }
        OnNPCBehaviourOver.Invoke();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
