using System;
using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    Queue<Sentence> sentences;

    public GameObject canvas;
    public GameObject button;

    public TextMeshProUGUI Player_nameText;
    public Image Player_face;
    public TextMeshProUGUI dialogueText;

    public List<Character> charactersInScene;

    public Animator animator;

    public float typeSpeed = 0.02f;

    Dialogue currentDialogue;

    public int currentSentenceIndex;

    public bool isTypingText;
    public PauseMenuManager pauseMenu;
    void Start()
    {
        if (charactersInScene == null)
        {
            Debug.LogError("attenzione, la lista di personaggi � vuotaaa");
        }
        currentSentenceIndex = 0;
        pauseMenu = FindAnyObjectByType<PauseMenuManager>();
        sentences = new Queue<Sentence>();
    }

    void Update()
    {
        if (!isTypingText && currentSentenceIndex == 0)
        {
            sentences.Clear();

        }
        if (Input.GetMouseButtonDown(0) && CanStartDialogue())
        {
            
            DisplayNextSentence();
        }
    }

    private bool CanStartDialogue()
    {
        return button.activeSelf == true && !pauseMenu.GetIsPaused();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        canvas.SetActive(true);
        button.SetActive(true);

        currentDialogue = dialogue;

        currentSentenceIndex = 0;
        OpenDialogueBox();

        DisplayNextSentence();
    }


    public void DisplayNextSentence()
    {
        if (IsDialogueOver() == false)
        {
            if (isTypingText)
            {
                WriteFullSentence();
            }
            else
            {
                TypeSentence(Sentence());
                CheckForEvent();
                currentSentenceIndex++;
            }
        }
        else
        {
            if (isTypingText)
            {
                WriteFullSentence();
            }
            else
            {
                EndDialogue();
                CloseDialogueBox();
            }

        }


       // ho provato a implementare il consiglio che ci ha dato
       // il pettinato per questa ridondanza, ma il metodo type sentcence diventava
       // irraggiungibile, onde evitare di rompere tutto lascio cos�

    }

    private void CheckForEvent()
    {
        if (IsThereEvent())
        {
            ExecuteSentenceEvent();
        }
    }

    private void WriteFullSentence()
    {
        StopAllCoroutines();
        ManageSingleSentenceSituation();

        isTypingText = false;
    }

    private void ManageSingleSentenceSituation()
    {
        if (currentSentenceIndex > 0)
        {
            dialogueText.text = DisplayThisSentence();
        }
        else
        {
            dialogueText.text = currentDialogue.sentences[0].text;
        }
    }

    private string DisplayThisSentence()
    {
        return currentDialogue.sentences[currentSentenceIndex - 1].text;
    }

    private void OpenDialogueBox()
    {
        animator.SetBool("IsOpen", true);
    }
    private void CloseDialogueBox()
    {
        animator.SetBool("IsOpen", false);
    }

    private void ExecuteSentenceEvent()
    {
        currentDialogue.sentences[currentSentenceIndex].onEndSentence.Invoke();
    }

    private bool IsThereEvent()
    {
        return currentDialogue.sentences[currentSentenceIndex].onEndSentence != null;
    }

    private bool IsDialogueOver()
    {
        return !(currentSentenceIndex < currentDialogue.sentences.Length);
    }

    private Sentence Sentence()
    {
        return currentDialogue.sentences[currentSentenceIndex];
    }

    private void TypeSentence(Sentence sentence)
    {
        if (sentence == null)
        {
            // Handle the null case, log an error, or return early.
            return;
        }
        Player_face.sprite = null;
        Player_nameText.text = null;
        
        KillOldImages(sentence);
        
        if (sentence.whoIsTalkingFace == null) 
        {
            int count = 0;
            foreach (Character character in charactersInScene)
            {

                if (sentence.whoIsTalking == character.Name)
                {
                    Player_face.sprite = character.face;
                    Player_nameText.text = character.Name;
                    count = 0;
                    break;
                }
                else
                {
                    count++;  
                }
                if (count >= charactersInScene.Count)
                {
                    Player_nameText.text = sentence.whoIsTalking;
                    Player_face.sprite = null;                  
                }

            }
        }
        if (sentence.whoIsTalkingFace != null)
        {
            Debug.LogWarning("ehi, c'� un immagine residua");
        }

        StartCoroutine(TypeSentence(sentence.text));
    }

    private static void KillOldImages(Sentence sentence)
    {
        sentence.whoIsTalkingFace = null;
    }

    IEnumerator TypeSentence(string sentence)
    {
        isTypingText = true;

        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;

            yield return new WaitForSeconds(typeSpeed);
        }

        isTypingText = false;
    }

    public void ExitDialogue()
    {
        sentences.Clear(); // Rimuovi tutte le frasi rimanenti
        EndDialogue();
    }

    public void EndDialogue()
    {
        if (currentDialogue.OnDialogueOver != null)
        {
            currentDialogue.OnDialogueOver.Invoke();
        }
       
        animator.SetBool("IsOpen", false);

        canvas.SetActive(false);
        button.SetActive(false);

    }
}