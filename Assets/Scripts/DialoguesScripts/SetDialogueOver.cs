/*using UnityEngine;
using UnityEngine.Events;
using System;
using System.Reflection;

public class SetDialogueOver : MonoBehaviour
{
    public InventoryItem eventSetup;
    public DialogueTrigger onEnd;

    void Start()
    {
        RegisterDynamicListener();
    }

    private void RegisterDynamicListener()
    {
        GameObject targetObject = GameObject.Find(eventSetup.item.gameObjectName);

        if (targetObject != null)
        {
            MonoBehaviour targetScript = targetObject.GetComponent(eventSetup.item.gameObjectComponent) as MonoBehaviour;

            if (targetScript != null)
            {
                MethodInfo targetMethod = targetScript.GetType().GetMethod(eventSetup.item.gameObjectMethod);

                if (targetMethod != null)
                {
                    // Debug
                    Debug.Log("Target Method: " + targetMethod.Name);

                    // Creazione di un oggetto contenente il valore del parametro
                    var parameterContainer = new { pippo = eventSetup.item.gameObjectParameter };

                    // Debug
                    Debug.Log("Parameter Value: " + parameterContainer.pippo);

                    // Creazione del delegato per il metodo generico
                    Action<object> dynamicDelegate = parameter => DynamicMethodInvokerGeneric(parameter, targetScript, targetMethod);

                    // Converti esplicitamente il delegato in UnityAction prima di aggiungerlo all'evento
                    UnityAction unityActionDelegate = () => dynamicDelegate(parameterContainer);
                    onEnd.dialogue.OnDialogueOver.AddListener(unityActionDelegate);
                }
                else
                {
                    Debug.LogError("Target Method is null.");
                }
            }
            else
            {
                Debug.LogError("Target Script is null.");
            }
        }
        else
        {
            Debug.LogError("Target Object not found.");
        }
    }

    private void DynamicMethodInvokerGeneric(object parameter, MonoBehaviour targetScript, MethodInfo targetMethod)
    {
        // Esegue il cast del parametro al tipo desiderato e invoca il metodo
        targetMethod.Invoke(targetScript, new object[] { Convert.ChangeType(parameter.GetType().GetProperty("pippo").GetValue(parameter), targetMethod.GetParameters()[0].ParameterType) });
    }
}
*/