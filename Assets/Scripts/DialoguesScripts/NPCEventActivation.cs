using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NPCEventActivation : MonoBehaviour
{ 
    public OpenActionMenu cellRaycast;
    public bool onClicked = false;
    public bool isTherePlayer = false;
    public bool isRunning = false;
    public NPCMovement nPCMovement;
    public UnityEvent onInteraction;

    private void OnMouseEnter()
    {
        cellRaycast.val = true;
    }

    private void OnMouseExit()
    {
        cellRaycast.val = false;
    }

    private void Start()
    {
        nPCMovement = FindObjectOfType<NPCMovement>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && cellRaycast.val)
        {
                onClicked = true;
        }
        else if (Input.GetMouseButtonDown(1) && !cellRaycast.val)
        {
                onClicked = false;
        }

        if (onClicked)
        {
            if (isTherePlayer && !isRunning) 
            {
                onInteraction.Invoke();
                onClicked = false;
            }
        }

        isRunning = nPCMovement.GetIsRunnig();
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            isTherePlayer = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            isTherePlayer = false;
        }
    }
}

