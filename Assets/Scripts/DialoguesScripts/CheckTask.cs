using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CheckTask : MonoBehaviour
{
    public GameObject[] gameObjectArray;
    public UnityEvent onAllTrue;
    public UnityEvent onAllFalse;
    public UnityEvent onMixed;

    public void CheckGameObjectArray()
    {
        if (gameObjectArray == null || gameObjectArray.Length == 0)
        {
            Debug.LogWarning("Array is null or empty.");
            return;
        }

        if (AllTrue())
        {
            onAllTrue.Invoke();
        }
        else if (AllFalse())
        {
            onAllFalse.Invoke();
        }
        else
        {
            onMixed.Invoke();
        }
    }

    bool AllTrue()
    {
        foreach (GameObject obj in gameObjectArray)
        {
            if (!obj.activeSelf)
                return false;
        }
        return true;
    }

    bool AllFalse()
    {
        foreach (GameObject obj in gameObjectArray)
        {
            if (obj.activeSelf)
                return false;
        }
        return true;
    }
}
