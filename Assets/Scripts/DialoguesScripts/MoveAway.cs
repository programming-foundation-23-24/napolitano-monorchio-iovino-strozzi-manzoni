using UnityEngine;

public class MoveAway : MonoBehaviour
{
    public float moveSpeed;
    public Transform playerAvoiding;
    public float X;
    public float Y;
    public bool lerp;
    public bool eventCondition;
    Vector2 targetPosition;

    public void Update()
    {
        if (gameObject.CompareTag("wall") || eventCondition)
        {
            if (Mathf.Approximately(transform.position.y, playerAvoiding.position.y) && Mathf.Approximately(transform.position.x, playerAvoiding.position.x))
            {
                lerp = true;
                targetPosition = new Vector2(transform.position.x + X, transform.position.y + Y);
            }

            if (lerp)
            {
                playerAvoiding.position = Vector2.MoveTowards(playerAvoiding.position, targetPosition, moveSpeed * Time.deltaTime);

                if (Vector2.Distance(playerAvoiding.position, targetPosition) < 0.01f)
                {
                    lerp = false;
                    eventCondition = false;
                }
            }
        }
    }
    public void OnEventCell()
    {
        if (Mathf.Approximately(transform.position.y, playerAvoiding.position.y) && Mathf.Approximately(transform.position.x, playerAvoiding.position.x))
        {
            eventCondition = true;
        }
    }
}
