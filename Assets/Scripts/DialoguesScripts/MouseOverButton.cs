using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class MouseOverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool isMouseOver = false;

    // Metodo chiamato quando il mouse entra nel bottone
    public void OnPointerEnter(PointerEventData eventData)
    {
        isMouseOver = true;
        
    }

    // Metodo chiamato quando il mouse esce dal bottone
    public void OnPointerExit(PointerEventData eventData)
    {
        isMouseOver = false;
       
    }
    public bool GetIsMouseOver()
    {
        return isMouseOver;
    }

    
}

