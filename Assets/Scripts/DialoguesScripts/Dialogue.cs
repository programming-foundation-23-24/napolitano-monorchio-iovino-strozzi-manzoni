using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Dialogue
{
    public Sentence[] sentences;
    public UnityEvent OnDialogueOver;
}

[System.Serializable]
public class Sentence
{
    public string whoIsTalking;
    [HideInInspector] public Sprite whoIsTalkingFace = null; //rendere di nuovo visibile questo campo per espressioni diverse
    public UnityEvent onEndSentence;
    [TextArea(3, 10)]
    public string text;
}




