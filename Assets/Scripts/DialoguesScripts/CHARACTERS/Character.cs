using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Character")]
public class Character : ScriptableObject
{
    public string Name;
    public Sprite face;
  

}
