using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    public AudioSource audioSource; // Riferimento all'AudioSource per il suono

    // Funzione per riprodurre il suono
    public void PlaySound()
    {
        if (audioSource != null)
        {
            audioSource.Play(); // Avvia la riproduzione del suono
        }
        else
        {
            Debug.LogWarning("AudioSource non assegnato al SoundController!");
        }
    }

    // Funzione per fermare il suono
    public void StopSound()
    {
        if (audioSource != null)
        {
            audioSource.Stop(); // Ferma la riproduzione del suono
        }
        else
        {
            Debug.LogWarning("AudioSource non assegnato al SoundController!");
        }
    }
}

