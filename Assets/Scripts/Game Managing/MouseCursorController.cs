using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MouseCursorController : MonoBehaviour
{
    public Texture2D CanExamineCursor;
    public Texture2D CantExamineCursor;
    public Texture2D CanWalkCursor;
    public Texture2D CantWalkCursor;
    public Texture2D CanClickCursor;
    public Texture2D CantClickCursor;
    Texture2D currentCursorTexture;
    public AudioSource clickSound;
     bool canChangeCursor = false;
    
     GameObject[] dialogueObjects;
    
     GameObject[] actionMenuObjects;
    
     Canvas[] actionMenuCanvas;
    

private void Update()
    {
        UpdateArraysContent();

        UpdateCursor();
        if (currentCursorTexture == CanClickCursor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                clickSound.Play();

            }
        }
    }

    private void UpdateArraysContent()
    {
        dialogueObjects = GameObject.FindGameObjectsWithTag("DialogueStuff");
        actionMenuObjects = GameObject.FindGameObjectsWithTag("ActionMenuPrefab");
        if (actionMenuObjects.Length > 0 && dialogueObjects.Length == 0)
        {
            foreach (GameObject dialogueObject in actionMenuObjects)
            {
                actionMenuCanvas = dialogueObject.GetComponents<Canvas>();
            }
        }
        else
        {
            actionMenuCanvas = null;
        }
    }

    private void UpdateCursor()
    {
        Vector2 mouseWorldPosition = GetMousePosition();
        Collider2D hitCollider = ShootColliderFromMouse(mouseWorldPosition);
        HandleMouseSituations(hitCollider);
    }

    private void HandleMouseSituations(Collider2D hitCollider)
    {
        if (hitCollider != null && hitCollider.gameObject.tag != "Block")
        {
            GameObject gameObjectsCollider = hitCollider.gameObject;

            AllowCursorChanging(hitCollider);

            if (HitsSomethingClickable(hitCollider))
            {
                SetCursor(CantClickCursor);
            }


            if (isExaminable(hitCollider))
            {
                SetCursor(CanExamineCursor);
            }



            if (IsNotInDialogue())
            {
                if (canChangeCursor)
                {
                    if (CanClickThis(hitCollider))
                    {
                        SetCursor(CanClickCursor);
                    }
                }
                else
                {
                    if (IsThisANotClickableUIElement(hitCollider) )
                    {
                        SetCursor(CantClickCursor);
                    }
                    else
                    {
                        HandleMouseOverObject(gameObjectsCollider);
                    }
                }
            }
            else if (IsInDialogue())
            {
                if (canChangeCursor)
                {
                    SetCursor(CanClickCursor);
                }
                else
                {
                    SetCursor(CantClickCursor);
                }
            }
        }
        else
        {
            if (IsInDialogue())
            {
                SetCursor(CantClickCursor);
            }
            else
            {
                SetCursor(CantWalkCursor);
            }
        }
    }

    //metodi incapsulati qua sotto
    private static bool HitsSomethingClickable(Collider2D hitCollider)
    {
        return hitCollider.gameObject.tag == "HUD" || hitCollider.gameObject.tag == "PieceOfPaper" || hitCollider.gameObject.tag == "Spot" || hitCollider.gameObject.tag == "ThingThatMoves" || hitCollider.gameObject.tag == "Molecule"; ;
    }

    private bool IsThisANotClickableUIElement(Collider2D hitCollider)
    {
        return actionMenuCanvas != null && actionMenuCanvas.Length > 0 && hitCollider.gameObject.GetComponent<Canvas>() != null;
    }

    private static bool isExaminable(Collider2D hitCollider)
    {
        return hitCollider.gameObject.GetComponent<InventoryItem>() != null && hitCollider.gameObject.GetComponent<InventoryItem>().item.actionMenu;
    }

    private void AllowCursorChanging(Collider2D hitCollider)
    {
        if (HasHitSomethingInteractable(hitCollider))
        {
            canChangeCursor = true;
        }
        else
        {
            canChangeCursor = false;
        }
    }

    private static bool CanClickThis(Collider2D hitCollider)
    {
        return hitCollider.gameObject.GetComponent<Button>().interactable;
    }

    private static bool HasHitSomethingInteractable(Collider2D hitCollider)
    {
        return (hitCollider.gameObject.GetComponent<Button>() != null || hitCollider.gameObject.GetComponent<InputField>() != null) && hitCollider.gameObject.GetComponentInParent<Canvas>().enabled;
    }

    private bool IsNotInDialogue()
    {
        return dialogueObjects.Length == 0;
    }

    private void HandleMouseOverObject(GameObject interactableObject)
    {
        
        RectGridCell_Viz rectGridCellViz = interactableObject.GetComponent<RectGridCell_Viz>();
        if (rectGridCellViz != null)
        {
            if (CellIsNotWalkable(rectGridCellViz))
            {
                SetCursor(CantWalkCursor);
            }
            else
            {
                HandleRectGridCell(rectGridCellViz);
            }
        }
    }

    private bool IsInDialogue()
    {
        return dialogueObjects.Length > 0;
    }
    private static Collider2D ShootColliderFromMouse(Vector2 mouseWorldPosition)
    {
        return Physics2D.OverlapPoint(mouseWorldPosition);
    }
    private static Vector2 GetMousePosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
    private void HandleRectGridCell(RectGridCell_Viz rectGridCellViz)
    {
        if (IsObjectExaminable(rectGridCellViz))
        {
            SetCursor(CanExamineCursor);
        }
        else if (IsObjectNotExaminable(rectGridCellViz))
        {
            SetCursor(CantExamineCursor);
        }
        else
        {
            SetCursor(CanWalkCursor);
        }
    }
    private static bool CellIsNotWalkable(RectGridCell_Viz rectGridCellViz)
    {
        return (rectGridCellViz.transform.Find("ActionMenuPrefab") == null && rectGridCellViz.transform.Find("NPC") == null) && rectGridCellViz.tag == "wall";
    }

    private static bool IsObjectNotExaminable(RectGridCell_Viz rectGridCellViz)
    {
        return !rectGridCellViz.GetComponent<OpenActionMenu>().enabled && (rectGridCellViz.transform.Find("ActionMenuPrefab") != null || rectGridCellViz.transform.Find("NPC") != null) && rectGridCellViz.tag != "Untagged";
    }

    private bool IsObjectExaminable(RectGridCell_Viz rectGridCellViz)
    {
        return (rectGridCellViz.transform.Find("ActionMenuPrefab") != null ||
                    (rectGridCellViz.transform.Find("Exit") != null && rectGridCellViz.transform.Find("Exit").gameObject.activeSelf) ||
                    (rectGridCellViz.transform.Find("NPC") != null && rectGridCellViz.transform.Find("NPC").gameObject.activeSelf))
                    && rectGridCellViz.GetComponent<OpenActionMenu>().enabled && dialogueObjects.Length == 0;
    }

    public void SetCursor(Texture2D cursorTexture)
    {
        currentCursorTexture = cursorTexture;
        if (cursorTexture == CantClickCursor || cursorTexture == CanClickCursor)
        {
            Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto); // Assuming hotspot is always at the center

        }
        else
        {
            Vector2 cursorHotspot = new Vector2(cursorTexture.width * 0.5f, cursorTexture.height * 0.5f);
            Cursor.SetCursor(cursorTexture, cursorHotspot, CursorMode.Auto); // Assuming hotspot is always at the center
        }
    }

}
