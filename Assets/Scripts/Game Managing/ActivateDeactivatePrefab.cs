using UnityEngine;

public class ActivateDeactivatePrefabs : MonoBehaviour
{
    public Canvas[] prefabsInScene;
    public string tagDaCercare;
    GameObject[] oggettiConTag;

    void Update()
    {
        oggettiConTag = GameObject.FindGameObjectsWithTag(tagDaCercare);

        if (oggettiConTag.Length > 0)
        {
            foreach (Canvas prefab in prefabsInScene)
            {
                if (prefab != null)
                {
                    prefab.enabled = false;
                }
            }
        }
        else
        {
            foreach (Canvas prefab in prefabsInScene)
            {
                if (prefab != null)
                {
                    prefab.enabled = true;
                }
            }
        }
    }
}