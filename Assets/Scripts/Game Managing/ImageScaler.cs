using UnityEngine;
using UnityEngine.UI;

public class ImageScaler : MonoBehaviour
{
    [SerializeField] private Image imageComponent;
    [SerializeField] private float targetAspectRatio = 1.0f;
    public float variabile;
    public Transform children;
    private void Start()
    {
        if (imageComponent == null)
        {
            // Cerca automaticamente l'immagine all'interno del GameObject
            imageComponent = GetComponent<Image>();
        }

        if (imageComponent == null)
        {
            Debug.LogError("Non trovo l'immagine.");
        }
    }

    private void Update()
    {
        Scale();
    }

    private void Scale()
    {
        if (imageComponent.sprite.rect.width > imageComponent.sprite.rect.height)
        {
            variabile = imageComponent.sprite.rect.height / imageComponent.sprite.rect.width;
            children.localScale = new Vector3(1f, variabile, 1.0f);
        }
        else if (imageComponent.sprite.rect.width < imageComponent.sprite.rect.height)
        {
            variabile = imageComponent.sprite.rect.width / imageComponent.sprite.rect.height;
            children.localScale = new Vector3(variabile, 1f, 1.0f);
        }

    }
}