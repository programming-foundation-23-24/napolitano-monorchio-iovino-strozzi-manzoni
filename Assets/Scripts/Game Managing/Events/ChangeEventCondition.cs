using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChangeEventCondition : MonoBehaviour
{
    public UnityEvent OnBoolChanged;
    public GameObject Condition;

    private bool previousConditionState;

    void Start()
    {
        previousConditionState = Condition.activeSelf;
    }

    public void Update()
    {
        if (Condition.activeSelf != previousConditionState)
        {
            previousConditionState = Condition.activeSelf;
            OnBoolChanged.Invoke();
        }
    }
}
