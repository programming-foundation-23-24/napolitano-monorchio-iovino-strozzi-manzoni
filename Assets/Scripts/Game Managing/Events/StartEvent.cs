using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartEvent : MonoBehaviour
{
    public DialogueTrigger dialogueToTrigger;


    public void OnEnable()
    {
        StartCoroutine(WaitToStart());
    }
    IEnumerator WaitToStart()
    {
        yield return new WaitForEndOfFrame();
        if (dialogueToTrigger != null)
        {
            dialogueToTrigger.TriggerDialogue();
        }
    }
    public void Suicide()
    {
        Destroy(gameObject);
    }
}