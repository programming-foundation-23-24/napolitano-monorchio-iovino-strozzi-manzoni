using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEnterExitEvent : MonoBehaviour
{

    public UnityEvent triggerEnter;
    public UnityEvent triggerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        triggerEnter.Invoke();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        triggerExit.Invoke();
    }

}
