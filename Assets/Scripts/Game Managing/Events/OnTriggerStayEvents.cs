using UnityEngine;
using UnityEngine.Events;

public class OnTriggerStayEvents : MonoBehaviour
{
    public UnityEvent onTriggerEnter;

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verifica se l'oggetto attuale ha un trigger con un altro oggetto
        if (other.CompareTag("Player")) // Sostituisci "YourTag" con il tag desiderato
        {
            // Esegui l'Unity Event quando c'� un trigger con l'altro oggetto
            onTriggerEnter.Invoke();
        }
    }
}
