using UnityEngine;

public class ActivateOnEvent : MonoBehaviour
{
    public MonoBehaviour[] scriptsToDisable;

    public void DisableScriptArray()
    {
        // Iterate through the array and disable each script
        foreach (MonoBehaviour script in scriptsToDisable)
        {
            if (script != null) // Check if the script exists to avoid errors
            {
                script.enabled = false;
            }
        }
    }
    public void ActivateScriptArray()
    {
        // Iterate through the array and disable each script
        foreach (MonoBehaviour script in scriptsToDisable)
        {
            if (script != null) // Check if the script exists to avoid errors
            {
                script.enabled = true;
            }
        }
    }

}

