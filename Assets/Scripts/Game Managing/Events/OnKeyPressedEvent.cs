using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnKeyPressedEvent : MonoBehaviour
{
    public KeyCode keyToPress;
    public UnityEvent onKeyPressed;

    void Update()
    {
        // Verifica se il tasto desiderato � stato premuto
        if (Input.GetKeyDown(keyToPress))
        {
            // Esegui l'evento quando il tasto viene premuto
            OnKeyPress();
        }
    }

    public void OnKeyPress()
    {
        onKeyPressed.Invoke();
    }
}
