using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    public bool trigger;
    public bool isRunning;
    public NPCMovement nPCMovement;
    public UnityEvent triggerEvent;

void Update()
    {
        isRunning = nPCMovement.GetIsRunnig();
        if (trigger && !isRunning)
        {
            triggerEvent.Invoke();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        trigger = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        trigger = false;
    }

}
