using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartFadeIn : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    public GameObject child;
    private float fadeInAlpha;
    public float fadeDuration;
    public GameObject parent;

    void OnEnable()
    {
        fadeInAlpha = 1f;
        canvasGroup = child.GetComponent<CanvasGroup>();
        canvasGroup.alpha = fadeInAlpha;
        StartCoroutine(FadeInCoroutine());
    }

    private IEnumerator FadeInCoroutine()
    {
        while (fadeInAlpha >= 0f)
        {
            fadeInAlpha -= Time.deltaTime / fadeDuration;
            canvasGroup.alpha = fadeInAlpha;
            yield return null;
        }

        parent.SetActive(false); // Disabilita l'oggetto dopo il fade in

        StopAllCoroutines();
    }
}
