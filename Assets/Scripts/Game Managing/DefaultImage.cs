using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefaultImage : MonoBehaviour
{
    [SerializeField] private Image imageComponent;

    // Update is called once per frame
    void Update()
    {
        if (imageComponent.sprite == null)
        {
            Color newColor = imageComponent.color;
            newColor.a = 0f;
            imageComponent.color = newColor;
        }
        else
        {
            Color newColor = imageComponent.color;
            newColor.a = 255f;
            imageComponent.color = newColor;

        }
    }
}
