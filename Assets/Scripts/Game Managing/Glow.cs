using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Glow : MonoBehaviour
{
    public Color hoverColor = Color.red; // Colore impostabile dall'Inspector
    public float lerpSpeed = 5f; // Velocit� del cambiamento di colore impostabile dall'Inspector

    private SpriteRenderer spriteRenderer;
    private Color originalColor;

    // Start is called before the first frame update
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalColor = spriteRenderer.color;
    }

    // Update is called once per frame
    void Update()
    {
        // Controlla se il mouse � sopra al gameObject padre
        if (IsMouseOverParent())
        {
            // Cambia gradualmente il colore con LERP
            spriteRenderer.color = Color.Lerp(spriteRenderer.color, hoverColor, Time.deltaTime * lerpSpeed);
        }
        else
        {
            // Se il mouse non � sopra, torna gradualmente al colore originale
            spriteRenderer.color = Color.Lerp(spriteRenderer.color, originalColor, Time.deltaTime * lerpSpeed);
        }
    }

    // Verifica se il mouse � sopra al gameObject padre
    private bool IsMouseOverParent()
    {
        // L'oggetto � sotto il mouse se il raycast colpisce il collider dell'oggetto padre
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        return hit.collider != null && hit.collider.gameObject.transform == transform.parent;
    }
}
