using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class FadeIn : MonoBehaviour
{
    public float fadeDuration = 2f; // Durata del fade in/out in secondi
    private CanvasGroup canvasGroup;
    private float fadeInAlpha = 1f; // Opacit� iniziale per il fade-in
    private float fadeOutAlpha = 0f; // Opacit� iniziale per il fade-out
    public GameObject child;
    public GameObject parent;
    public UnityEvent OnFadeOutOver;
    public UnityEvent OnFadeInOver;

    private void Start()
    {
        canvasGroup = child.GetComponent<CanvasGroup>();
        canvasGroup.alpha = fadeInAlpha;
    }

    public void StartFadeIn()
    {
        StartCoroutine(FadeInCoroutine());
    }

    public void StartFadeIn2()
    {
        StartCoroutine(FadeInCoroutine2());
    }

    public void StartFadeOut()
    {
        StartCoroutine(FadeOutCoroutine());
    }
    

    private IEnumerator FadeInCoroutine()
    {
        while (fadeInAlpha > 0f)
        {
            fadeInAlpha -= Time.deltaTime / fadeDuration;
            canvasGroup.alpha = fadeInAlpha;
            yield return null;
        }

        child.SetActive(false); // Disabilita l'oggetto dopo il fade in
        parent.SetActive(false); // Disabilita l'oggetto dopo il fade in
    }
    
    private IEnumerator FadeInCoroutine2()
    {
        while (fadeInAlpha > 0f)
        {
            fadeInAlpha -= Time.deltaTime / fadeDuration;
            canvasGroup.alpha = fadeInAlpha;
            yield return null;
        }
        OnFadeInOver.Invoke();
        yield return null;

        child.SetActive(false); // Disabilita l'oggetto dopo il fade in
        varReset();
    }
        
    private void varReset()
    {
        fadeInAlpha = 1;
        canvasGroup.alpha = fadeInAlpha;
    }

    private IEnumerator FadeOutCoroutine()
    {
        child.SetActive(true); // Assicurati che l'oggetto sia attivo prima di iniziare il fade-out

        while (fadeOutAlpha < 1f)
        {
            fadeOutAlpha += Time.deltaTime / fadeDuration;
            canvasGroup.alpha = fadeOutAlpha;
            yield return null;
        }
        OnFadeOutOver.Invoke();
        yield return null;

    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
