using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SetWallOrUntagged : MonoBehaviour
{
    public GameObject[] cells;
    public GameObject[] cellsToSet;
    public string[] startingCellTags;
    private string tag;
    public UnityEvent onNeedToActivate;

    public bool needToActivate = false;

    public int j = 0;

    void Start()
    {
        tag = gameObject.tag;
    }

    void Update()
    {
        if (!needToActivate)
        {
            CheckForTag();
        }

        CheckForDifference();
    }

    private void CheckForDifference()
    {
        for (int i = 0; i < cells.Length; i++)
        {

            if (cells[i].gameObject.tag != tag)
            {
                j++;
            }
        }

        if (j == cells.Length)
        {
            needToActivate = true;
            onNeedToActivate.Invoke();
        }
    }

    public void CheckForTag()
    {
        if (gameObject.tag == "Untagged")
        {
            foreach (var cell in cells)
            {
                cell.gameObject.tag = "Untagged";
            }
        }

        if (gameObject.tag == "wall")
        {
            foreach (var cell in cells)
            {
                cell.gameObject.tag = "wall";
            }
        }
    }

    public void DeactivateScript()
    {
        enabled = false;
    }
}
