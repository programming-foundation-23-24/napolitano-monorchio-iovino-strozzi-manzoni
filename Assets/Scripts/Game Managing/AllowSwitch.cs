using System.Collections;
using System.Collections.Generic;
using System.Xml.Schema;
using Unity.VisualScripting;
using UnityEngine;

public class AllowSwitch : MonoBehaviour
{
    public AttivaDisattivaScript shift;
    int sceltedafare = 0;

    private void Start()
    {
        if (shift != null)
        {
            shift = (AttivaDisattivaScript)FindAnyObjectByType(typeof(AttivaDisattivaScript));
        }
    }
    private void Update()
    {
        DoTheThingy();
    }

    private void DoTheThingy()
    {
        if (sceltedafare >= 5)
        {
            shift.SetCanSwitch(true);
            Debug.Log("Hey premi spazio per cambiare personaggio");
        }
    }

    public void increaseCount() // da refractorare meglio
    {
        sceltedafare += 1;
    }
}
