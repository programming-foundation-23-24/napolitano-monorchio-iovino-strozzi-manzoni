using UnityEngine;

public class ActivateDeactivateComponents : MonoBehaviour
{
    public MonoBehaviour[] componentsInScene;
    public string tagToSearch;
    public GameObject[] objectsWithTag;

    void Update()
    {
        objectsWithTag = GameObject.FindGameObjectsWithTag(tagToSearch);

        if (objectsWithTag.Length > 0)
        {
            foreach (MonoBehaviour component in componentsInScene)
            {
                component.enabled = false;
            }
        }
        else
        {
            foreach (MonoBehaviour component in componentsInScene)
            {
                component.enabled = true;
            }
        }
    }
}
