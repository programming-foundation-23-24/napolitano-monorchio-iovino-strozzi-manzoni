using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class TriggerController : MonoBehaviour
{
    public List<GameObject> oggettiDaAttivareDisattivare;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) // Assicurati che il tag del personaggio sia "Player" o modificane il controllo di confronto in base al tuo setup
        {
            foreach (GameObject oggetto in oggettiDaAttivareDisattivare)
            {
                if (oggetto.activeSelf) // Se l'oggetto è attivo, lo disattiva
                {
                    oggetto.SetActive(false);
                }
                else // Se l'oggetto è disattivo, lo attiva
                {
                    oggetto.SetActive(true);
                }
            }
        }
    }
}

