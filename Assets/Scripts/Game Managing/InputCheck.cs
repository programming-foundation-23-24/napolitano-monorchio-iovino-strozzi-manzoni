using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InputCheck : MonoBehaviour
{
    public UnityEvent onRight;
    public UnityEvent onWrong;
    public Text textCheck;
    public InputField inputCheck;
    public string solution;

    public void OnEnter()
    {
        if (textCheck.text.ToLower() == solution)
        {
            onRight.Invoke();
            inputCheck.text = "";
            textCheck.text = "";
        }
        else
        {
            onWrong.Invoke();
            inputCheck.text = "";
            textCheck.text = "";
        }

    }
}
