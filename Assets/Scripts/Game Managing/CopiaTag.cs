using UnityEngine;

public class CopiaTag : MonoBehaviour
{
    public GameObject oggettoDaCuiCopiareIlTag;

    void Update()
    {
        // Copia il tag se l'oggetto da cui copiare � presente
        if (oggettoDaCuiCopiareIlTag != null)
        {
            CopiaTagDaAltroOggetto();
        }
        else
        {
            Debug.LogWarning("Specificare l'oggetto da cui copiare il tag nel campo 'oggettoDaCuiCopiareIlTag'.");
        }
    }

    void CopiaTagDaAltroOggetto()
    {
        // Copia il tag
        gameObject.tag = oggettoDaCuiCopiareIlTag.tag;
    }
}