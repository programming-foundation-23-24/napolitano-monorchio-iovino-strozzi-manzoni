using UnityEngine;
using UnityEngine.UI;

public class AttivaDisattivaScript : MonoBehaviour
{
    public GameObject[] oggettiDaAttivareDisattivare;
    public NPCMovement[] nPCMovement;
    public bool[] isRunning;
    public Animator dialogueManagerAnimator;
    public bool isDialogueBoxOpen;
    public bool canSwitch = false;
    public int i = 0;
    public string tagToSearch;
    public GameObject[] objectsWithTag;
    public GameObject transition;
    public GameObject[] shiftButton;
    public GameObject[] shiftDisabledButton;


    void Update()
    {
        objectsWithTag = GameObject.FindGameObjectsWithTag(tagToSearch);

        if (Input.GetKeyDown(KeyCode.Space) && !isRunning[i] && !isDialogueBoxOpen && canSwitch)
        {
            AttivaDisattivaOggetti();
        }

        if (!isRunning[i] && !isDialogueBoxOpen && canSwitch)
        {
            foreach (GameObject button in shiftButton)
            {
                button.SetActive(true);
            }
            foreach (GameObject button in shiftDisabledButton)
            {
                button.SetActive(false);
            }
        }
        else
        {
            foreach (GameObject button in shiftButton)
            {
                button.SetActive(false);
            }
            foreach (GameObject button in shiftDisabledButton)
            {
                button.SetActive(true);
            }
        }

        IsDialogueopen();
        isRunning[0] = nPCMovement[0].GetIsRunnig();
        isRunning[1] = nPCMovement[1].GetIsRunnig();

    }
    public void OnShiftButtonClick()
    {
        if (!isRunning[i] && !isDialogueBoxOpen && canSwitch)
        {
            AttivaDisattivaOggetti();
        }
    }

    public void AttivaDisattivaOggetti()
    {
        transition.SetActive(true);

        if (i == 0)
        {
            i = 1;
        }
        else if (i == 1)
        {
            i = 0;
        }
        // Itera attraverso l'array degli oggetti
        foreach (GameObject oggetto in oggettiDaAttivareDisattivare)
        {
            // Verifica se l'oggetto � attivo e lo disattiva, o viceversa
            oggetto.SetActive(!oggetto.activeSelf);
        }
    }

    void IsDialogueopen()
    {
        isDialogueBoxOpen = dialogueManagerAnimator.GetBool("IsOpen");
    }

    public void SetCanSwitch(bool val)
    {
        canSwitch = val;
    }

    public void CleanActionMenu()  //metodo richiamato negli eventi e non negli script.
    {
        foreach (GameObject menu in objectsWithTag)
        {
            CloseActionMenu closeActionMenu = menu.GetComponent<CloseActionMenu>();
            if (closeActionMenu != null)
            {
                closeActionMenu.Deactivate();
            }
        }
    }
}