using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour
{
    // Start is called before the first frame update
    public string nomeScenaDaCaricare;

    // Metodo chiamato quando l'oggetto viene cliccato
    public void CaricaNuovaScena()
    {
        // Carica la scena specificata
        SceneManager.LoadScene(nomeScenaDaCaricare);
    }
    public void CaricaQuestaScena(string nome)
    {
        SceneManager.LoadScene(nome);
    }
}
