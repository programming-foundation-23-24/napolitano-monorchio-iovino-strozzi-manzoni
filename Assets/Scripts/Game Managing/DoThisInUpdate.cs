using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoThisInUpdate : MonoBehaviour
{

    public UnityEvent eventToDoInUpdate;
    void Update()
    {
        eventToDoInUpdate.Invoke();
    }
}
