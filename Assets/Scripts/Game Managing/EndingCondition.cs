using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

[System.Serializable]
public class CheckListElement
{
    public string variableName; 

    public bool value = false;
}

public class EndingCondition : MonoBehaviour
{
    public List<CheckListElement> checkList = new List<CheckListElement>();
    public UnityEvent onCheckListComplete;
    public bool allTrue = true;

    private void Update()
    {
        if (CheckIfListIsComplete())
        {
            onCheckListComplete.Invoke();
        }
    }
    public void SetVariableValue(string variableName)
    {
        CheckListElement element = checkList.Find(x => x.variableName == variableName);

        if (element != null)
        {
            element.value = true;
        }
        else
        {
            Debug.Log("Variable with name " + variableName + " not found in the checklist.");
        }
    }
    public void SetVariableValueFalse(string variableName) 
    {
        CheckListElement element = checkList.Find(x => x.variableName == variableName);

        if (element != null)
        {
            element.value = false;
        }
        else
        {
            Debug.Log("Variable with name " + variableName + " not found in the checklist.");
        }
    }


    private bool CheckIfListIsComplete()
    {
        int i = 0;
        foreach (var elements in checkList)
        {
            if (!elements.value)
            {
                i++;
            }
        }
        if (i > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
