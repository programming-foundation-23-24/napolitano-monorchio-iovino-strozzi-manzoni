using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour
{
    public InventoryManager inventoryManager;

    void Start()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
    }
    public ItemType CheckForItem(InventorySlot inventorySlot)
    {
        if (inventorySlot.transform.childCount == 0)
        {
            return null;
        }
        else
        {
            InventoryItem inventoryItem = inventorySlot.GetComponentInChildren<InventoryItem>();

            if (inventoryItem != null)
            {
                return inventoryItem.item; // Assuming 'item' is a reference to ItemType in your InventoryItem script
            }
            else
            {
                return null;
            }
        }
    }
    public void RemoveItem(int index)
    {
        // Rimuovi visivamente l'item dallo slot
        foreach (Transform child in transform)
        {
            inventoryManager.items[index] = null;
            Destroy(child.gameObject);

        }
    }
}
