using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveTake : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public ItemType item;

    void Start()
    {
        if (inventoryManager == null)
        {
            inventoryManager = FindObjectOfType<InventoryManager>();
        }
    }

    public void Take()
    {
        if (inventoryManager != null)
        {
            inventoryManager.DestroyItem(item);
        }
    }
    public void Give()
    {
        if (inventoryManager != null)
        {
            inventoryManager.AddItem(item);
        }
    }
}
