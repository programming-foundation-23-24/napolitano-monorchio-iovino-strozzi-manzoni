using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CombineThroughItem : MonoBehaviour
{
    public InventoryItem itemOnInventory;
    public ItemType item;
    public UnityEvent onConditionOver;

    void Update()
    {
        if (itemOnInventory.item == item)
        {
            onConditionOver.Invoke();
        }
    }
}
