using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddItemTest : MonoBehaviour
{

    public InventoryManager inventoryManager;
    public ItemType[] itemsToPickUp;


    // Start is called before the first frame update
    public void PickUpItem(int id)
    {
        Debug.Log("Trying to pick up item with ID: " + id);
        bool result = inventoryManager.AddItem(itemsToPickUp[id]);
        if(result)
        {
            Debug.Log("added");
        }
        else 
        {
            Debug.Log("full");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            PickUpItem(0);
        }
        else if(Input.GetKeyDown(KeyCode.E))
        {
            PickUpItem(1);
        }
        else if(Input.GetKeyDown(KeyCode.R))
        {
            PickUpItem(2);
        }
    }
}
