using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using System;
using System.Reflection;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Scriptale object/Item")]
public class ItemType : ScriptableObject
{
    public Sprite image;
    public string ExamineText = "TESTO ESAMINA QUI ";
    public bool actionMenu;
    public bool[] actionButtons;
    public Dialogue dialogue;
}
