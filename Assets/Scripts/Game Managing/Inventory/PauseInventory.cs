using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class PauseInventory : MonoBehaviour
{
    public bool isInventoryOpen;
    public GameObject inventory;
    public GameObject button;

    void OnMouseEnter()
    {
        isInventoryOpen = true;
    }

    void OnMouseExit()
    {
        isInventoryOpen = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && !isInventoryOpen)
        {
            inventory.SetActive(false);
            button.SetActive(true);
        }
    }
}
