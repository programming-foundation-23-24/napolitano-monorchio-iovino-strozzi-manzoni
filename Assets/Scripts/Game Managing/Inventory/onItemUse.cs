using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class onItemUse : MonoBehaviour
{
    public InventoryItem[] itemOnInventory;
    public ItemType item;
    public bool isThereItem;
    public bool isItemDestroyed;
    public UnityEvent onItemDestroyed;

    void Update()
    {
        itemOnInventory = FindObjectsOfType<InventoryItem>();
        if (!isThereItem)
        {
            foreach (InventoryItem items in itemOnInventory)
            {
                if (items.item == item)
                {
                    isThereItem = true;
                }
            }
        }
        CheckItemDestroyed();
    }
    void CheckItemDestroyed()
    {
        if (isThereItem && !IsItemInInventory())
        {
            // L'oggetto � stato distrutto, attiva l'Unity Event
            onItemDestroyed.Invoke();
            isThereItem = false; // Imposta nuovamente a falso per ricercare l'oggetto nella prossima iterazione
        }
    }
    bool IsItemInInventory()
    {
        // Controlla se l'oggetto � ancora nell'inventario
        foreach (InventoryItem items in itemOnInventory)
        {
            if (items.item == item)
            {
                return true;
            }
        }
        return false;
    }
}
