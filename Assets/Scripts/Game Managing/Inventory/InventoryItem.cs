using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class InventoryItem : MonoBehaviour
{

    [HideInInspector] public Transform parentAfterDrag;
    DialogueManager dialogueManager;
    InventoryManager inventoryManager;
    [HideInInspector] public ItemType item;
    public ActionMenu actionMenu;
    public Image image;
    public bool condition;
    public GameObject[] dialogueStuff;
    public Character Jacob;
    public Character Kaylee;

    void Start()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        inventoryManager = FindObjectOfType<InventoryManager>();
        if (item != null && actionMenu != null && actionMenu.childActiveStates != null && item.actionButtons != null)
        {
            int iterations = Mathf.Min(actionMenu.childActiveStates.Length, item.actionButtons.Length);
            for (int i = 0; i < iterations; i++)
            {
                actionMenu.childActiveStates[i] = item.actionButtons[i];
            }
        }
    }

    private void Update()
    {
        dialogueStuff = GameObject.FindGameObjectsWithTag("DialogueStuff");

        if (dialogueStuff.Length == 0)
        {
            condition = false;
        }

        if (condition)
        {
            if(inventoryManager.gameObject.tag == Jacob.Name)
            {
                dialogueManager.Player_face.sprite = Jacob.face;
                dialogueManager.Player_nameText.text = Jacob.Name;
            }
            else if(inventoryManager.gameObject.tag == Kaylee.Name)
            {
                dialogueManager.Player_face.sprite = Kaylee.face;
                dialogueManager.Player_nameText.text = Kaylee.Name;
            }
        }
    }

    public void InitialiseItem(ItemType newItem)
    {
        if (newItem != null)
        {
            item = newItem;
            image.sprite = newItem.image;            
        }
        else
        {
            item = null;
            image.sprite = null;
        }
    }
    public void TriggerItemDialogue()
    {
        dialogueManager.StartDialogue(item.dialogue);
        StartCoroutine(WaitAndSetCondition());
    }

    IEnumerator WaitAndSetCondition()
    {
        yield return null;
        condition = true;
        StopAllCoroutines();
    }

    public void SetConditionFalse()
    {
        condition = false;
    }

    public void SetConditionTrue()
    {
        StartCoroutine(WaitAndSetCondition());
    }
}