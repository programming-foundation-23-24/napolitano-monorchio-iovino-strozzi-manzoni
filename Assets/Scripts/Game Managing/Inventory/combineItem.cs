using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class combineItem : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public ItemType[] DestroyItem;
    public ItemType[] AddItem;

    void OnEnable()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();

    }

    // Update is called once per frame
    public void Combine()
    {
        for (int i = 0; i < DestroyItem.Length; i++)
        {
            inventoryManager.DestroyItem(DestroyItem[i]);
        }

        for (int i = 0; i < AddItem.Length; i++)
        {
            inventoryManager.AddItem(AddItem[i]);
        }
    }
}
