using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public ItemType emptySlot;
    public List<InventorySlot> inventorySlotList; // Cambiato da array a lista
    public List<ItemType> items; // Cambiato da array a lista
    public GameObject InventoryItemPrefab;

    public bool AddItem(ItemType item)
    {
        Debug.Log("Adding item: " + item.name);

        if (TryGetFirstEmptySlotIndex(out int index))
        {
            SpawnNewItem(item, inventorySlotList[index]);
            return true;
        }

        return false;
    }

    void SpawnNewItem(ItemType item, InventorySlot slot)
    {
        StartCoroutine(SpawnNewItemNextFrame(item, slot));
    }

    IEnumerator SpawnNewItemNextFrame(ItemType item, InventorySlot slot)
    {
        yield return null; // Aspetta un frame

        GameObject newItemGo = Instantiate(InventoryItemPrefab, slot.transform);
        InventoryItem inventoryItem = newItemGo.GetComponent<InventoryItem>();
        inventoryItem.InitialiseItem(item);

        // Puoi arrestare la coroutine dopo l'istanziazione dell'oggetto
        StopCoroutine("SpawnNewItemNextFrame");
    }

    public void GetInventoryList()
    {
        int j = 0;
        foreach (InventorySlot slot in inventorySlotList)
        {
            ItemType itemInSlot = slot.CheckForItem(slot);
            if (itemInSlot != emptySlot)
            {
                items[j] = itemInSlot;
                j++;
            }
        }
    }

    public void DestroyItem(ItemType itemToDestroy)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] != null && items[i].Equals(itemToDestroy))
            {
                inventorySlotList[i].RemoveItem(i);
                items[i] = emptySlot;

                print("ho distrutto " + itemToDestroy);
                print("ora alla posizione di " + i + " c'� " + items[i]);

                if (items[i] != emptySlot)
                {
                    print("ehi, mi rendo conto che non � null...");
                }

            }
        }
    }

    public bool TryGetFirstEmptySlotIndex(out int index)
    {
        for (int i = 0; i < items.Count; i++)
        {
            ItemType item = items[i];
            if (item == emptySlot)
            {
                index = i;
                return true;
            }
        }

        index = -1;
        return false;
    }

    public bool HasInventoryEmptySlot()
    {
        foreach (ItemType item in items)
        {
            if (item == emptySlot)
            {
                return true;
            }
        }

        return false;
    }

    void RemoveChildlessFathers()
    {
        for (int i = 0; i < inventorySlotList.Count; i++)
        {
            GameObject slot = inventorySlotList[i].gameObject;

            if (slot.transform.childCount == 0)
            {
                items[i] = emptySlot;
            }
        }
    }

    void SlideInInventory()
    {
        for (int i = 0; i < inventorySlotList.Count - 1; i++)
        {
            InventorySlot currentSlot = inventorySlotList[i];
            InventorySlot nextSlot = inventorySlotList[i + 1];

            if (currentSlot.transform.childCount == 2)
            {
                Transform secondChild = currentSlot.transform.GetChild(1);

                while (nextSlot.transform.childCount > 0)
                {
                    i++;
                    nextSlot = inventorySlotList[i + 1];

                    if (i == inventorySlotList.Count - 1)
                    {
                        break;
                    }
                }

                if (nextSlot.transform.childCount == 0)
                {
                    secondChild.SetParent(nextSlot.transform);
                    secondChild.localPosition = Vector3.zero;
                }
            }
        }
    }

    void SlideNonEmptyItemsToEmptySlots()
    {
        for (int i = 0; i < inventorySlotList.Count - 1; i++)
        {
            InventorySlot currentSlot = inventorySlotList[i];
            InventorySlot nextSlot = inventorySlotList[i + 1];

            if (currentSlot.transform.childCount == 0 && nextSlot.transform.childCount > 0)
            {
                Transform child = nextSlot.transform.GetChild(0);

                while (i > 0 && inventorySlotList[i - 1].transform.childCount == 0)
                {
                    i--; 
                    currentSlot = inventorySlotList[i];
                }

                if (currentSlot.transform.childCount == 0 && i >= 0)
                {
                    child.SetParent(currentSlot.transform);
                    child.localPosition = Vector3.zero;
                }
            }
        }  
    }

    private void Update()
    {
        GetInventoryList();
        RemoveChildlessFathers();
        SlideInInventory();
        SlideNonEmptyItemsToEmptySlots();
    }
}