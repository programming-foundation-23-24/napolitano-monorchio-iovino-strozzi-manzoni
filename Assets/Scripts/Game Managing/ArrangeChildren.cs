using UnityEngine;

public class ArrangeChildren : MonoBehaviour
{
    public float cellWidth = 1f; // Larghezza di ogni cella
    public float cellHeight = 1f; // Altezza di ogni cella
    public Vector2 spacing = new Vector2(1f, 1f); // Spaziatura tra le celle
    public bool autoArrangeOnStart = true; // Se true, arranger� i figli all'avvio del gioco
    public RenameChildren cellRename;
    int maxRows;
    int maxColumns;

    void OnDrawGizmos()
    {
        cellRename = gameObject.GetComponent<RenameChildren>();
        if (cellRename != null)
        {
            maxColumns = cellRename.maxColumns;
            maxRows = cellRename.maxRows;
        }
        if (autoArrangeOnStart)
            Arrange();
    }

    [ContextMenu("Arrange Children")]
    void Arrange()
    {
        int rowCount = maxColumns;
        int columnCount = maxRows;

        int index = 0;
        for (int row = 0; row < rowCount; row++)
        {
            float xPos = row * (cellHeight + spacing.y);

            for (int column = 0; column < columnCount; column++)
            {
                if (index >= transform.childCount)
                    return;

                Transform child = transform.GetChild(index);
                float yPos = column * (cellWidth + spacing.x);

                child.localPosition = new Vector3(xPos, yPos, 0f);
                index++;
            }
        }
    }
}
