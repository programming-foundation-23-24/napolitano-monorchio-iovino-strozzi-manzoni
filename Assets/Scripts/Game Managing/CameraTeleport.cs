using UnityEngine;
using Cinemachine;

public class CameraTeleport : MonoBehaviour
{
    public Transform jacob; // Primo punto di teletrasporto
    public Transform kaylee; // Secondo punto di teletrasporto
    public GameObject detective; // Secondo punto di teletrasporto
    public GameObject killer; // Secondo punto di teletrasporto
    public AttivaDisattivaScript conditions; // Secondo punto di teletrasporto
    public CinemachineVirtualCamera playerposition; // Secondo punto di teletrasporto

    private bool isTeleportingToTarget1 = true; // Flag per tener traccia del punto di teletrasporto attivo

    void Update()
    {
        killer = GameObject.FindGameObjectWithTag("Killer");
        detective = GameObject.FindGameObjectWithTag("Detective");
        conditions = FindFirstObjectByType<AttivaDisattivaScript>();

        // Se il tasto spazio viene premuto, cambia il punto di teletrasporto attivo
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if ((!conditions.isRunning[0] || !conditions.isRunning[1]) && !conditions.isDialogueBoxOpen && conditions.canSwitch)
            {
                if(killer != null && detective == null)
                {
                    transform.position = kaylee.position;
                    playerposition.Follow = kaylee;
                }
                else if(killer == null && detective != null)
                {
                    transform.position = jacob.position;
                    playerposition.Follow = jacob;
                }
            }
        }
    }
}