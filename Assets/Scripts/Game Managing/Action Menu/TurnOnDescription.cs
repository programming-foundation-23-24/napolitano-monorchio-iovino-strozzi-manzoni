using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnDescription : MonoBehaviour
{

    public GameObject description;

    public bool isMouseOverDaddy;
    public GameObject[] dialogueStuff;
    public GameObject actionMenu;
    public GameObject NPC;
    OpenActionMenu openActionMenu;
    bool isMouseOver;

    void Start()
    {
        FindActionMenu();
        FindDescription(this.gameObject.transform);
        openActionMenu = gameObject.GetComponent<OpenActionMenu>();
    }
    void OnEnable()
    {
        OnMouseOver();
        isMouseOverDaddy = false;
    }
    public void SearchNPC()
    {
        if (NPC == null)
        {
            if (transform.Find("NPC").gameObject != null)
            {
                NPC = transform.Find("NPC").gameObject;
            }
        }
    }
    void FindActionMenu()
    {
        foreach (Transform child in transform)
        {
            if (child.CompareTag("ActionMenuPrefab"))
            {
                if (child.gameObject != null)
                {
                    actionMenu = child.gameObject;
                    break;
                }
            }
        }
    }
    void FindDescription(Transform parent)
    {

        foreach (Transform child in parent)
        {
            if (child.CompareTag("Description"))
            {
                description = child.gameObject;
                return; // Non � necessario continuare la ricerca se abbiamo trovato il GameObject
            }

            // Se il figlio ha altri figli, continuiamo la ricerca in profondit�
            if (child.childCount > 0)
            {
                FindDescription(child);
                // Se abbiamo trovato il GameObject durante la ricerca in profondit�, usciamo dalla funzione
                if (description != null)
                    return;
            }
        }
    }

    void OnMouseOver()
    {
        isMouseOverDaddy = true;
    }

   
    void OnMouseExit()
    {
        isMouseOverDaddy = false;
    }
    void Update()
    {

        if (isMouseOverDaddy) 
        { 
            isMouseOver = true; 
        }
        else 
        { 
            isMouseOver = false;
        }

        dialogueStuff = GameObject.FindGameObjectsWithTag("DialogueStuff");

        if (description != null && actionMenu != null && openActionMenu != null)
        {
            if (isMouseOver && dialogueStuff.Length == 0 && !actionMenu.activeSelf && openActionMenu.enabled)
            {
                description.SetActive(true);
            }
            else
            {
                description.SetActive(false);
            }

            if (Input.GetKeyDown("space"))
            {
                description.SetActive(false);
            }
        }
        
        if (description != null && NPC != null)
        {
            if (isMouseOver && dialogueStuff.Length == 0 && NPC.activeSelf && openActionMenu.enabled)
            {
                description.SetActive(true);
            }
            else
            {
                description.SetActive(false);
            }

            if (Input.GetKeyDown("space"))
            {
                description.SetActive(false);
            }
        }
    }
}
