using UnityEngine;

public class CloseItemMenu : MonoBehaviour
{
    public GameObject oggettoPadre;
    public bool val = false;
    public OpenItemMenu setOnClicked;

    private void OnMouseEnter()
    {
        val = true;
    }

    public void OnMouseExit()
    {
        val = false;
    }

    private void Update()
    {
        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) && !val)
        {
            if (oggettoPadre.activeSelf)
            {
                // Ottieni tutti i collider2D figli
                Collider2D[] childColliders = oggettoPadre.GetComponentsInChildren<Collider2D>();

                // Controlla se il mouse non colpisce nessun collider2D figlio
                bool mouseNotOverChildColliders = true;
                foreach (var childCollider in childColliders)
                {
                    Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (childCollider.OverlapPoint(mousePosition))
                    {
                        mouseNotOverChildColliders = false;
                        break;
                    }
                }

                // Se il mouse non colpisce nessun collider2D figlio, disattiva l'oggetto padre
                if (mouseNotOverChildColliders)
                {
                    Deactivate();
                }
            }
        }
    }
    public void OnDisable()
    {
        oggettoPadre.SetActive(false);
        setOnClicked.onClicked = false;
    }
    public void Deactivate()
    {
        oggettoPadre.SetActive(false);
        setOnClicked.onClicked = false;
    }
}
