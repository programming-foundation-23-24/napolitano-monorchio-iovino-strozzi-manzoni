using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Use : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public InventoryItem itemsToDestroy;

    void Start()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
    }

    public void UseObject()
    {
        inventoryManager.DestroyItem(itemsToDestroy.item);
    }
    public void GiveObject()
    {
        inventoryManager.AddItem(itemsToDestroy.item);
    }
}
