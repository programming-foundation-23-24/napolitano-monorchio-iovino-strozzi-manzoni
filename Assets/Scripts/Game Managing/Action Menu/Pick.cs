using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pick : MonoBehaviour
{
    public InventoryManager inventoryManager;
    public ItemType itemsToPickUp;
    public ActionMenu action;
    public GameObject prefab;

    private bool hasPickedUp = false;

    public void PickUpItem()
    {
        if (!hasPickedUp)
        {
            bool result = inventoryManager.AddItem(itemsToPickUp);
            if (result)
            {
                if (action != null)
                {
                    action.childActiveStates[3] = false;
                }
                if (transform.parent != null && transform.parent.tag != "DialogueStuff")
                {
                    Transform parentTransform = transform.parent;
                    parentTransform.gameObject.tag = "Untagged";
                }
                hasPickedUp = true;
            }
        }
    }
    public void DestroyObject()
    {
        if (prefab != null)
        {
            Destroy(prefab);
        }
    }
}