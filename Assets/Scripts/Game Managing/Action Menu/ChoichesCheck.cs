using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ChoicesCheck : MonoBehaviour
{
    bool HasClickedEverything = false;
    public GameObject[] HasClicked;
    public UnityEvent AllOptionsClicked;
    // Update is called once per frame
    void Update()
    {
        CheckHasClicked();
    }

    void CheckHasClicked()
    {
        HasClickedEverything = true; // Assume all objects are clicked initially

        foreach (GameObject item in HasClicked)
        {
            if (!item.activeSelf)
            {
                HasClickedEverything = false;
                break; // Exit the loop if any object is not active
            }
        }

        // Now HasClickedEverything will be true only if all objects are clicked
        if (HasClickedEverything)
        {
            AllOptionsClicked.Invoke();
        }
    }
}
