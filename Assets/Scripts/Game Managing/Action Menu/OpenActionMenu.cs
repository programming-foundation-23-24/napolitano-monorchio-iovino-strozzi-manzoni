using UnityEngine;
using GameAI.PathFinding;
using System.Collections;
using System.Collections.Generic;

public class OpenActionMenu : MonoBehaviour
{
    public GameObject oggettoFiglio;
    public bool val = false;
    public bool onClicked = false;
    public bool isTherePlayer = false;
    public bool isRunning = false;
    public NPCMovement nPCMovement;

    private void OnMouseEnter()
    {
        val = true;
    }

    private void OnMouseExit()
    {
        val = false;
    }

    public void SearchActionMunu()
    {
        Transform childTransform = transform.GetChild(0);

        if (childTransform != null && childTransform.tag == "ActionMenuPrefab")
        {
            oggettoFiglio = childTransform.gameObject;
        }
    }
    private void Start()
    {
        nPCMovement = FindObjectOfType<NPCMovement>();
        Transform childTransform = transform.GetChild(0);
        if (childTransform != null && childTransform.tag == "ActionMenuPrefab")
        {
            oggettoFiglio = childTransform.gameObject;
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1) && val)
        {
            // Aggiungi una verifica per assicurarti che l'oggettoFiglio non sia nullo
            if (oggettoFiglio != null && !oggettoFiglio.activeSelf)
            {
                onClicked = true;
            }
        }
        else if (Input.GetMouseButtonDown(1) && !val)
        {
            // Aggiungi una verifica per assicurarti che l'oggettoFiglio non sia nullo
            if (oggettoFiglio != null && !oggettoFiglio.activeSelf)
            {
                onClicked = false;
            }
        }

        if (onClicked)
        {
            if (isTherePlayer && !isRunning) { AttivaOggettoFiglio();}
            
        }
        else 
        {
            DisattivaOggettoFiglio();     
        }

        if (isRunning)
        {
            isTherePlayer = false;
        }

        isRunning = nPCMovement.GetIsRunnig();
    }

    public void OnTriggerStay2D(Collider2D collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            isTherePlayer = true;
        }
    }

    private void AttivaOggettoFiglio()
    {
        if (oggettoFiglio != null)
        {
            oggettoFiglio.SetActive(true);
        }
    }

    private void DisattivaOggettoFiglio()
    {
        if (oggettoFiglio != null)
        {
            oggettoFiglio.SetActive(false);
        }
    }
    
    public void ExitButton()
    {
        if (oggettoFiglio != null)
        {
            onClicked = false;
        }
    }
    
    public void OnDisable()
    {
        onClicked = false;
        isTherePlayer = false;
        val = false;
    }
}
