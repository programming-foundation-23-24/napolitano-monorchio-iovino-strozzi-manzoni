using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public GameObject objectToMove;
    public string tagName;
    public void ObjectToMove()
    {
        objectToMove.gameObject.tag = tagName;

    }
}