using UnityEngine;

public class CloseActionMenu : MonoBehaviour
{
    public GameObject oggettoPadre;
    public bool val = false;
    public OpenActionMenu setOnClicked;
    public GameObject[] dialogueStuff;

    private void OnMouseEnter()
    {
        val = true;
    }

    private void OnMouseExit()
    {
        val = false;
    }

    private void Update()
    {
        dialogueStuff = GameObject.FindGameObjectsWithTag("DialogueStuff");

        if (dialogueStuff.Length > 0)
        {
            transform.GetComponent<Collider2D>().enabled = false;
            foreach (Collider2D collider2D in transform.GetComponentsInChildren<Collider2D>())
            {
                collider2D.enabled = false;
            }
        }
        else
        {
            transform.GetComponent<Collider2D>().enabled = true;
            foreach (Collider2D collider2D in transform.GetComponentsInChildren<Collider2D>())
            {
                collider2D.enabled = true;
            }
        }

        if ((Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) && !val && dialogueStuff.Length == 0)
        {
            // Verifica se l'oggetto figlio � attualmente disattivato
            if (oggettoPadre.activeSelf)
            {
                // Ottieni tutti i collider2D figli
                Collider2D[] childColliders = oggettoPadre.GetComponentsInChildren<Collider2D>();

                // Controlla se il mouse non colpisce nessun collider2D figlio
                bool mouseNotOverChildColliders = true;
                foreach (var childCollider in childColliders)
                {
                    Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (childCollider.OverlapPoint(mousePosition))
                    {
                        mouseNotOverChildColliders = false;
                        break;
                    }
                }

                // Se il mouse non colpisce nessun collider2D figlio, disattiva l'oggetto padre
                if (mouseNotOverChildColliders)
                {
                    Deactivate();
                }
            }
        }
    }

    public void Deactivate() 
    { 
        oggettoPadre.SetActive(false);
        setOnClicked.onClicked = false;
    }
    
}
