﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OpenItemMenu : MonoBehaviour
{
    public GameObject oggettoFiglio;
    public GameObject menuFiglio;
    public bool mouseOver = false;
    public bool onClicked = false;
    public InventoryItem inventoryItem;
    public TextMeshProUGUI testoEsamina;
    //riferimento a oggetto di tipo TextMeshPro tipo public TextMeshPro testoEsamina;
    private void Start()
    {
        if (testoEsamina == null)
        {
            Debug.LogWarning("Il componente TextMeshProUGUI non è stato trovato sull'oggetto con tag 'ExamineText'.");
        }
    }
    private void OnMouseEnter()
    {
        mouseOver = true;
    }

    private void OnMouseExit()
    {
        mouseOver = false;
    }

    private void Update()
    {
        if (mouseOver)
        {
            if (inventoryItem != null && inventoryItem.item != null)
            {
                
                if (testoEsamina != null)
                {

                    testoEsamina.text = inventoryItem.item.ExamineText;
                    oggettoFiglio.SetActive(true);
                    
                }
            }           
        }
        else
        {
            testoEsamina.text = "";
            oggettoFiglio.SetActive(false);
        }
    
        if (mouseOver && Input.GetMouseButtonDown(1))
        {
            onClicked = true;
        }

        if (onClicked && inventoryItem != null && inventoryItem.item != null && inventoryItem.item.actionMenu)
        {
            menuFiglio.SetActive(true);
        }
    }
    public void ExitButton()
    {
        // Aggiungi una verifica per assicurarti che l'oggettoFiglio non sia nullo
        if (menuFiglio != null)
        {
            onClicked = false;
        }
    }
    public void OnDisable()
    {
        mouseOver = false;
    }
}    
