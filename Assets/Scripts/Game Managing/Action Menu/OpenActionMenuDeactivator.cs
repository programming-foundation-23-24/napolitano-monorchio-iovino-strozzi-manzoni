using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenActionMenuDeactivator : MonoBehaviour
{
    public GameObject dialogueStuff;
    public CloseActionMenu[] componentsInScene;
    void Update()
    {

        if (dialogueStuff.activeSelf)
        {
            foreach (CloseActionMenu component in componentsInScene)
            {
                if (component != null)
                {
                    component.enabled = false;
                }
            }
        }
        else
        {
            foreach (CloseActionMenu component in componentsInScene)
            {
                if (component != null)
                {
                    component.enabled = true;
                }
            }
        }
    }
}
