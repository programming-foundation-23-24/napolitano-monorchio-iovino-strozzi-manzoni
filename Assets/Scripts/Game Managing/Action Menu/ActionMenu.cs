using UnityEngine;
using UnityEngine.UI;

public class ActionMenu : LayoutGroup
{
    public float fDistance;
    [Range(0f, 360f)]
    public float MinAngle, MaxAngle, StartAngle;

    // Array di booleani per controllare lo stato di attivazione di ciascun child
    public bool[] childActiveStates;

    protected override void OnEnable()
    {
        base.OnEnable();
        CalculateRadial();
    }

    public override void SetLayoutHorizontal()
    {
    }

    public override void SetLayoutVertical()
    {
    }

    public override void CalculateLayoutInputVertical()
    {
        CalculateRadial();
    }

    public override void CalculateLayoutInputHorizontal()
    {
        CalculateRadial();
    }

    protected override void OnRectTransformDimensionsChange()
    {
        base.OnRectTransformDimensionsChange();
    }

    void LateUpdate()
    {
        CalculateRadial();
    }

    void CalculateRadial()
    {
        m_Tracker.Clear();
        int activeChildCount = 0;

        for (int i = 0; i < transform.childCount; i++)
        {
            RectTransform child = (RectTransform)transform.GetChild(i);
            if (child != null)
            {
                // Controlla se l'elemento corrente deve essere attivato o disattivato
                bool isActive = i < childActiveStates.Length ? childActiveStates[i] : true;

                activeChildCount++;

                // Aggiunge gli elementi al tracker per impedire agli utenti di modificare le loro posizioni tramite l'editor.
                m_Tracker.Add(this, child, DrivenTransformProperties.Anchors | DrivenTransformProperties.AnchoredPosition | DrivenTransformProperties.Pivot);

                // Calcola l'angolo in base al numero totale di figli attivi
                float angleRange = MaxAngle - MinAngle;
                float step = angleRange / (transform.childCount - 1);

                float fAngle = MinAngle + step * (activeChildCount - 1);
                Vector3 vPos = new Vector3(Mathf.Cos(fAngle * Mathf.Deg2Rad), Mathf.Sin(fAngle * Mathf.Deg2Rad), 0) * fDistance;

                child.localPosition = vPos;

                // Forza gli oggetti a essere centrati, questo pu� essere cambiato, ma suggerirei di mantenere tutti gli oggetti con gli stessi punti di ancoraggio.
                child.anchorMin = child.anchorMax = child.pivot = new Vector2(0.5f, 0.5f);

                // Imposta setActive in base allo stato del bool
                child.gameObject.SetActive(true);

                if (!isActive)
                {
                    if (child.gameObject.GetComponent<Button>() != null)
                    {
                        child.gameObject.GetComponent<Button>().interactable = false;
                    }
                    
                    if (child.gameObject.GetComponent<Buttons>() != null)
                    {
                        child.gameObject.GetComponent<Buttons>().enabled = false;
                    }
                }
                else
                {
                    if (child.gameObject.GetComponent<Button>() != null)
                    {
                        child.gameObject.GetComponent<Button>().interactable = true;
                    }

                    if (child.gameObject.GetComponent<Buttons>() != null)
                    {
                        child.gameObject.GetComponent<Buttons>().enabled = true;
                    }
                }
            }
        }
    }
}
