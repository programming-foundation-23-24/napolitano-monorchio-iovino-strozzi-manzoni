using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArrayControl
{
    public ItemType itemDaControllare;
    public GameObject siAttiva;
}

public class IsThereItemScript : MonoBehaviour
{
    List<ItemType> inventoryItems;
    public List<ArrayControl> arrayToControl; // Ora � una lista

    InventoryManager inventoryManager;

    void Update()
    {
        foreach (ArrayControl arrayControl in arrayToControl)
        {
            IsThereItem(arrayControl.itemDaControllare, arrayControl.siAttiva);
        }
    }

    public void IsThereItem(ItemType item, GameObject siAttiva)
    {
        bool found = inventoryItems != null && inventoryItems.Contains(item);

        if (!found && siAttiva != null)
        {
            siAttiva.SetActive(false);
        }
        else if (siAttiva != null)
        {
            siAttiva.SetActive(true);
        }
    }

    private void OnEnable()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
        if (inventoryManager != null)
        {
            inventoryItems = inventoryManager.items;
        }
        else
        {
            Debug.LogError("InventoryManager non trovato!");
        }
    }
}
