using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Combine : MonoBehaviour
{
    public UnityEvent onHaving;
    InventoryManager inventoryManager;
    List<ItemType> inventoryItems; // Cambiato da array a lista
    public GameObject siAttiva;
    public ItemType itemDaControllare;
    public GameObject prefab;

    public void CombineObject(ItemType itemToCheck)
    {
        int itemCount = 0;

        if (inventoryItems != null)
        {
            foreach (ItemType item in inventoryItems)
            {
                if (item == itemToCheck)
                {
                    itemCount++;
                }
            }

            if (itemCount > 0)
            {
                onHaving.Invoke();
            }

        }
    }

    public void IsThereItem(ItemType item)
    {
        bool found = inventoryItems != null && inventoryItems.Contains(item);

        if (!found && siAttiva != null)
        {
            siAttiva.SetActive(false);
        }
        else if (siAttiva != null)
        {
            siAttiva.SetActive(true);
        }
    }

    private void OnEnable()
    {
        inventoryManager = FindObjectOfType<InventoryManager>();
        if (inventoryManager != null)
        {
            inventoryItems = inventoryManager.items;
        }
        else
        {
            Debug.LogError("InventoryManager non trovato!");
        }
    }

    private void Update()
    {
        IsThereItem(itemDaControllare);
    }

    public void DestroyObject()
    {
        if (prefab != null)
        {
            Destroy(prefab);
        }
    }
}
