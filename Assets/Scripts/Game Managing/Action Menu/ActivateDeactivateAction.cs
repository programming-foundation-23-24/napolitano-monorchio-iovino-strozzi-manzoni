using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateDeactivateAction : MonoBehaviour
{
    public int[] actionToDeactivate;
    public int[] actionToActivate;
    public ActionMenu action;

    public void Activate()
    {
        foreach (int obj in actionToActivate)
        {
            action.childActiveStates[obj] = true;
        }
    }
    public void Deactivate()
    {
        foreach (int obj in actionToDeactivate)
        {
            action.childActiveStates[obj] = false;
        }

    }
}
