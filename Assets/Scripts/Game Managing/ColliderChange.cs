using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderChange : MonoBehaviour
{
    public BoxCollider2D[] boxColliders;
    private string tagIniziale;
    public bool blockUpdate;

    void Start()
    {
        tagIniziale = gameObject.tag;
        if (boxColliders.Length == 0)
        {
            boxColliders = GetComponents<BoxCollider2D>();
        }

    }

    void Update()
    {
        if (!blockUpdate)
        {
            if (gameObject.tag != tagIniziale)
            {
                foreach (BoxCollider2D collider in boxColliders)
                {
                    collider.enabled = !collider.enabled;
                }
                tagIniziale = gameObject.tag;
            }
        }
    }
    public void ChangeByEvent()
    {
            foreach (BoxCollider2D collider in boxColliders)
            {
                collider.enabled = !collider.enabled;
            }
    }
}