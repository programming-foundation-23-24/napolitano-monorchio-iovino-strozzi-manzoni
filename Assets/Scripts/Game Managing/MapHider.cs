using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MapHider : MonoBehaviour
{
    public List<SpriteRenderer> childSpriteRenderers = new List<SpriteRenderer>();
    public List<Collider2D> childcollider2d = new List<Collider2D>();
    private SpriteRenderer spriteRenderer;
    public UnityEvent superUsefulEvent;
    public UnityEvent onObject1Enter;
    public UnityEvent onObject2Enter;
    public GameObject object1;
    public GameObject object2;
    bool doTheSuperUsefulEvent = true;

    public void Start()
    {
        GetChildSpriteRenderers(transform);
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
    }

    private void GetChildSpriteRenderers(Transform parent)
    {
        foreach (Transform child in parent)
        {
            SpriteRenderer renderer = child.GetComponent<SpriteRenderer>();
            Collider2D collider = child.GetComponent<Collider2D>();
            if (renderer != null)
            {
                childSpriteRenderers.Add(renderer);
            }

            if (collider != null)
            {
                childcollider2d.Add(collider);
            }

            if (child.childCount > 0)
            {
                GetChildSpriteRenderers(child);
            }
        }
    }

    public void TriggerEnter()
    {
        if (doTheSuperUsefulEvent)
        {
            superUsefulEvent.Invoke();
            doTheSuperUsefulEvent = false;
        }

        if (spriteRenderer != null)
        {
            spriteRenderer.enabled = false;
        }

        foreach (SpriteRenderer renderer in childSpriteRenderers)
        {
            if (renderer != null) // Add null check
            {
                renderer.enabled = false;
            }
        }

        foreach (Collider2D collider in childcollider2d)
        {
            if (collider != null) // Add null check
            {
                collider.enabled = false;
            }
        }

        if (object1.activeSelf && !object2.activeSelf)
        {
            onObject1Enter.Invoke();
        }
        else if (!object1.activeSelf && object2.activeSelf)
        {
            onObject2Enter.Invoke();
        }
    }

    public void TriggerExit()
    {

        if (spriteRenderer != null)
        {
            spriteRenderer.enabled = true;
        }

        foreach (SpriteRenderer renderer in childSpriteRenderers)
        {
            if (renderer != null) // Add null check
            {
                renderer.enabled = true;
            }
        }

        foreach (Collider2D collider in childcollider2d)
        {
            if (collider != null) // Add null check
            {
                collider.enabled = true;
            }
        }
    }
}
