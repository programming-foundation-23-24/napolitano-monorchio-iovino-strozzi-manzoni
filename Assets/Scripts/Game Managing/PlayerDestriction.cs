using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDestriction : MonoBehaviour
{
    public GameObject description;

    public bool isMouseOverDaddy;
    public GameObject[] dialogueStuff;
    bool isMouseOver; 

    void Start()
    {
        FindDescription(this.gameObject.transform);
    }
    void OnEnable()
    {
        OnMouseOver();
        isMouseOverDaddy = false;
    }
    void FindDescription(Transform parent)
    {

        foreach (Transform child in parent)
        {
            if (child.CompareTag("Description"))
            {
                description = child.gameObject;
                return;
            }

            
            if (child.childCount > 0)
            {
                FindDescription(child);
                
                if (description != null)
                    return;
            }
        }
    }

    void OnMouseOver()
    {
        isMouseOverDaddy = true;
    }

    void OnMouseExit()
    {
        isMouseOverDaddy = false;
    }

    void Update()
    {
        if (isMouseOverDaddy)
        {
            isMouseOver = true;
        }
        else
        {
            isMouseOver = false;
        }
        dialogueStuff = GameObject.FindGameObjectsWithTag("DialogueStuff");
        if (description != null)
        {
            if (isMouseOver && dialogueStuff.Length == 0)
            {
                description.SetActive(true);
            }
            else
            {
                description.SetActive(false);
            }

            if (Input.GetKeyDown("space"))
            {
                description.SetActive(false);
            }
        }
    }
}
