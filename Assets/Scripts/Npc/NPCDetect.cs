using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDetect : MonoBehaviour
{
    public Collider2D[] colliders;
    public bool previousTagState;
    public bool currentTagState;
    public bool NPCinfluence;
    public string tagName;
    public GameObject nPC;
    public GameObject npcPrefab;
    public GameObject npcParent;

    void Start()
    {
        NPCinfluence = false;
        UpdateNPCState();
        previousTagState = (transform.tag == tagName);

        if (transform.tag == tagName)
        {
            previousTagState = true;
        }
        else
        {
            previousTagState = false;
        }

        if (nPC == null)
        {
            nPC = transform.Find("NPC").gameObject;
        }
    }

    void Update()
    {
        if (transform.tag == tagName)
        {
            currentTagState = true;
        }
        else
        {
            currentTagState = false;
        }

        UpdateNPCState();
    }

    void UpdateNPCState()
    {
        if (nPC != null)
        {
            if (!NPCinfluence)
            {
                nPC.gameObject.SetActive(previousTagState);
            }

            if (currentTagState != previousTagState)
            {
                previousTagState = currentTagState;
                foreach (Collider2D collider in colliders)
                {
                    collider.enabled = !collider.enabled;
                }
            }
        }
    }


    public void EventNPCState()
    {
        if (currentTagState != previousTagState)
        {
            previousTagState = currentTagState;
            foreach (Collider2D collider in colliders)
            {
                collider.enabled = !collider.enabled;
            }
        }
    }
    public void TrueInfluenceNPC()
    {
        NPCinfluence = true;
    }
    public void FalseInfluenceNPC()
    {
        NPCinfluence = false;
    }

    public void DeleteNPC()
    {
        if (nPC != null)
        {
            Destroy(nPC);
        }
    }

    public void AddNPC()
    {
        if (nPC == null)
        {
            GameObject newNPC = Instantiate(npcPrefab, transform.position, Quaternion.identity, npcParent.transform);
            newNPC.name = "NPC";
            nPC = newNPC;
        }
    }
}
