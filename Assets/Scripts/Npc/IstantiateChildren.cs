using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class InstantiateChildren : MonoBehaviour
{
    public GameObject prefabToInstantiate;
    public RenameChildren gridData;
    public bool generateGrid=false;

    void Awake()
    {
        gridData = GetComponent<RenameChildren>();
    }

    void OnDrawGizmos()
    {
        if (generateGrid && !Application.isPlaying)
        {
            GenerateGrid();
        }
    }
    void GenerateGrid()
    {

        if (prefabToInstantiate == null || gridData == null)
        {
            Debug.LogWarning("Prefab o gridData non assegnato.");
            return;
        }

        int totalCells = gridData.maxRows * gridData.maxColumns;

        
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }

        
        for (int i = 0; i < totalCells; i++)
        {
            
            int row = i / gridData.maxColumns;
            int column = i / gridData.maxColumns;
            Vector3 position = new Vector3(column, row, 0f);

            // Istanza il prefab come figlio
            //GameObject newChild = (GameObject)PrefabUtility.InstantiatePrefab(prefabToInstantiate, transform);
            //newChild.transform.localPosition = position;
            //newChild.name = "riga" + (row + 1).ToString() + "colonna" + column.ToString();
            generateGrid = false;
        }
    }
}

