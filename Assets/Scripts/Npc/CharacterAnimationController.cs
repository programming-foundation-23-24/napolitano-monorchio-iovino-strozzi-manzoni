using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    Animator animator;
    MovementDetector movementDetector;
    string status;
    public bool GoSlow = false;
    public float slowSpeed;
    void Start()
    {
        if (!animator)
        {
            animator = GetComponentInParent<Animator>();
        }
        if (!movementDetector)
        {
            movementDetector = GetComponentInParent<MovementDetector>();
        }

    }

    void Update()
    {
        status = movementDetector.GetStatus();
        if (GoSlow)
        {
            animator.speed = slowSpeed;
        }
        else
        {
            animator.speed = 1f;
        }
        if (status == "idle")
        {
            animator.Play("Idle");
        }
        if (status == "left")
        {
            animator.Play("Left");
        }
        if (status == "right")
        {
            animator.Play("Right");
        }
    }
    public void SetGoSlow(bool val)
    {
        GoSlow = val;
    }
}
