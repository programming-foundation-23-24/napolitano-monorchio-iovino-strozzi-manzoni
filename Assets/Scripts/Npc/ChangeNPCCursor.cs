using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeNPCCursor : MonoBehaviour
{
    public NPCEventActivation npc;
    public Transform npcGameObject;
    void Start()
    {
        npc = GetComponent<NPCEventActivation>();
        npcGameObject = npc.cellRaycast.gameObject.transform.Find("NPC");
    }
    void OnEnable()
    {
        if (npc != null) 
        { 
            npc.nPCMovement = FindObjectOfType<NPCMovement>();
        }
    }
    public void DestroyNPC()
    {
        if (npcGameObject != null) 
        {
            Destroy(npcGameObject.gameObject);
        }
    }

    public void SearchNPC()
    {
        npcGameObject = npc.cellRaycast.gameObject.transform.Find("NPC");
    }

    void Update()
    {
        if (npc != null) 
        { 
            if (!npc.enabled)
            {
                npc.cellRaycast.enabled = false;
            }
            else
            {
                npc.cellRaycast.enabled = true;
                npc.cellRaycast.nPCMovement = npc.nPCMovement;
            }
        }
    }
}
