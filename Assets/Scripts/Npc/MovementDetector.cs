using UnityEngine;

public class MovementDetector : MonoBehaviour
{
    Rigidbody2D rb;
    Vector2 previousPosition;
    public GameObject inpurReader;
    string status = "idle";
    Vector2 inputPosition;
    Vector2 inputDirection ;
    void Start()
    {
        if (!inpurReader)
        {
            Debug.LogError("manca l'input reader nell'inspector ");
        }
        
        rb = GetComponent<Rigidbody2D>();
        
        previousPosition = rb.position;
    }

    void Update()
    {
        inputPosition = inpurReader.transform.position;
        
        Vector2 inputDirection = inputPosition - (Vector2)transform.position;
        
        Vector2 currentPosition = rb.position;


        Vector2 movementDirection = (currentPosition - previousPosition);

        if ((currentPosition - previousPosition) == Vector2.zero)
        {
            status = "idle";
        }
        if (movementDirection.x > 0)
        {
            status = "right";
        }
        else if (movementDirection.x < 0)
        {
            status = "left";
        }

        if (movementDirection.y > 0)
        {
            CheckDirection();
        }
        else if (movementDirection.y < 0)
        {
            CheckDirection();
        }
        previousPosition = currentPosition;
    }

    private void CheckDirection()
    {
        Vector2 inputDirection = inputPosition - (Vector2)transform.position;

        float horizontalDistance = Mathf.Abs(inputDirection.x);
        float verticalDistance = Mathf.Abs(inputDirection.y);

        
        if (horizontalDistance > verticalDistance)
        {
            
            if (inputDirection.x < 0)
                status = "left";
            else
                status = "right";
        }
       
        else if (verticalDistance > horizontalDistance)
        {
            
            if (inputDirection.x < 0)
                status = "left";
            else
                status = "right";
        }
        
        else
        {
            
            if (status == "idle" && inputDirection.x < 0)
                status = "right";
            else if (status == "idle")
                status = "left";
        }
    }



    public string GetStatus()
    {
        return status;
    }
}
