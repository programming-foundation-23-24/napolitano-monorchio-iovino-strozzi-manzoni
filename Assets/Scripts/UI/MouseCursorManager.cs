using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MouseCursorManager : MonoBehaviour
{
    public Texture2D hoverCursorTexture;
    public Texture2D defaultCursorTexture;// La texture del cursore quando passa sopra al bottone


    public void OnMouseEnterButton()
    {
        //ChangeCursorTexture(hoverCursorTexture);
    }

    public void OnMouseExitButton()
    {
        //ChangeCursorTexture(defaultCursorTexture);
    }

    private void ChangeCursorTexture(Texture2D texture)
    {
        print("sto cambiando il cursore");
        Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
    }

    // Assicurati di ripristinare la texture del cursore quando il gioco viene arrestato
    private void OnApplicationQuit()
    {
        Cursor.SetCursor(defaultCursorTexture, Vector2.zero, CursorMode.Auto);
    }
}
