using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonDisabledText : MonoBehaviour
{
    public Button parent;
    public TextMeshProUGUI text;
    public TextMeshProUGUI text2;
    public Color textColor;
    public Color textColor2;

    void Start()
    {
        parent = gameObject.GetComponent<Button>();
        text = gameObject.transform.GetChild(0)?.GetComponentInChildren<TextMeshProUGUI>();
        text2 = gameObject.transform.GetChild(1)?.GetComponentInChildren<TextMeshProUGUI>();

        if (text != null)
            textColor = text.color;
        if (text2 != null)
            textColor2 = text2.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (!parent.interactable)
        {
            if (text != null)
            {
                textColor.a = 255f;
                ColorUtility.TryParseHtmlString("#000000", out textColor);
                text.color = textColor;
                text2.color = textColor;
            }
        }
        else
        {
            if (text != null)
            {
                textColor.a = 255f;
                ColorUtility.TryParseHtmlString("#696969", out textColor);
                text.color = textColor;
                text2.color = textColor2;
            }

        }
    }
}
