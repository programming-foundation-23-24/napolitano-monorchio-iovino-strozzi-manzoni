using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCanvasLayer : MonoBehaviour 
{
    public Canvas canvasPadre;
    public Canvas[] canvasFigli;
    void Start()
    {
        canvasPadre = GetComponentInParent<Canvas>();
    }
    
    void Update()
    {
        if (canvasPadre != null)
        {
            // Ottieni tutti i canvas figli
            canvasFigli = GetComponentsInChildren<Canvas>();

            // Imposta il sorting layer per ciascun canvas figlio
            for (int i = 0; i < canvasFigli.Length; i++)
            {
                canvasFigli[i].sortingLayerID = canvasPadre.sortingLayerID;
            }
        }
    }
}
