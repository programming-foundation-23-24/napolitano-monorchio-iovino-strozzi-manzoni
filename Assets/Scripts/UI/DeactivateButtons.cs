using UnityEngine;
using UnityEngine.UI;

public class DeactivateButtons : MonoBehaviour
{
    public GameObject[] buttons;
    public string tagDaDisattivare;

    void OnEnable()
    {
        buttons = GameObject.FindGameObjectsWithTag(tagDaDisattivare);

        foreach (GameObject button in buttons)
        {
            if (button != null)
            {
                button.SetActive(false);
            }
        }
    }
    void OnDisable()
    {
        foreach (GameObject button in buttons)
        {
            if (button != null)
            {
                button.SetActive(true);
            }
        }
    }
}
