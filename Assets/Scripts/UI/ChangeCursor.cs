using UnityEngine;

public class ChangeCursor : MonoBehaviour
{
    public Texture2D defaultCursorTexture; // Texture del cursore predefinita
    public Texture2D hoverCursorTexture; // Texture del cursore quando il mouse � sopra l'oggetto padre
    public Color hoverColor = Color.red; // Colore desiderato del cursore quando il mouse � sopra l'oggetto padre
    public float lerpSpeed = 5f; // Velocit� del cambiamento di colore impostabile dall'Inspector

    private bool isMouseOver = false;

    void Start()
    {
        Cursor.SetCursor(defaultCursorTexture, Vector2.zero, CursorMode.Auto); // Imposta la texture del cursore predefinita
    }

    void Update()
    {
        if (IsMouseOverParent())
        {
            // Cambia gradualmente il colore e la texture del cursore quando il mouse � sopra l'oggetto padre
            ChangeCursorColorAndTexture(hoverColor, hoverCursorTexture);
        }
        else
        {
            // Se il mouse non � sopra, torna gradualmente al colore e alla texture del cursore predefinita
            ChangeCursorColorAndTexture(Color.white, defaultCursorTexture);
        }
    }

    // Cambia il colore e la texture del cursore
    void ChangeCursorColorAndTexture(Color color, Texture2D texture)
    {
        Cursor.SetCursor(texture, Vector2.zero, CursorMode.Auto);
        GUI.color = color;
    }

    // Verifica se il mouse � sopra all'oggetto padre
    private bool IsMouseOverParent()
    {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        return hit.collider != null && hit.collider.gameObject.transform == transform.parent;
    }
}
