using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CopyButtontext : MonoBehaviour
{
    public TextMeshProUGUI otherText;
    public TextMeshProUGUI text;

    void Start()
    {
        text = transform.GetComponent<TextMeshProUGUI>();
        text.text = otherText.text;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
