using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChangeCursorForButtons : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public MouseCursorController mouseManager;

    private void Start()
    {
        mouseManager = FindAnyObjectByType<MouseCursorController>();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseManager.SetCursor(mouseManager.CanExamineCursor);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseManager.SetCursor(mouseManager.CanWalkCursor);
    }

   
}
