using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeactivateCanvas : MonoBehaviour
{
    public Canvas[] canvases;
    public string tagDaNonDisattivare;

    void OnEnable()
    {
        canvases = FindObjectsOfType<Canvas>();

        foreach (Canvas canvas in canvases)
        {
            if (canvas != null && canvas.gameObject.tag != tagDaNonDisattivare)
            {
                canvas.enabled = false;
            }
        }
    }
    void OnDisable()
    {
        foreach (Canvas canvas in canvases)
        {
            if (canvas != null && canvas.gameObject.tag != tagDaNonDisattivare)
            {
                canvas.enabled = true;
            }
        }
    }
}
