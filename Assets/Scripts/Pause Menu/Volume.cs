using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
    public Slider volumeSlider; // Trascina lo Slider dal tuo pannello di pausa nell'Inspector
    public AudioSource backgroundMusic; // Trascina l'Audio Source della tua musica di sottofondo nell'Inspector

    void Start()
    {
        // Imposta il valore dello Slider in base al volume attuale
        volumeSlider.value = backgroundMusic.volume;
    }

    // Metodo chiamato quando lo Slider viene modificato
    public void OnVolumeChange()
    {
        // Imposta il volume audio in base alla posizione dello Slider
        backgroundMusic.volume = volumeSlider.value;
    }
}

