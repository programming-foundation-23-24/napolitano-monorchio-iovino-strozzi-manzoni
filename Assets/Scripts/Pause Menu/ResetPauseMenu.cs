using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPauseMenu : MonoBehaviour
{

    public GameObject[] disable;
    public GameObject able;

    void OnEnable()
    {
        if (able != null)
        {
            able.SetActive(true);
        }

        foreach (GameObject var in disable)
        {
            if (var != null)
            {
                var.SetActive(false);
            }
        }
    }
}
