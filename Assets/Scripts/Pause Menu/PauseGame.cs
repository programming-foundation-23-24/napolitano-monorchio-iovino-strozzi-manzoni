using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject pauseMenu;
    public KeyCode pauseKey;
    public static bool isPaused;

    //AGGIUNGERE DI IGNORARE GLI INPUT MENTRE SI � IN PAUSA 

    void Start()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    // Update is called once per frame
    void Update()
    {

        if(PauseButton.onClickMenu)
        {
            resumeGame();
            PauseButton.onClickMenu = false;
            Debug.Log("godo");
        }

        if (Input.GetKeyDown(pauseKey))
        {
            if (isPaused)
            {
                resumeGame();
            }
            else
            {
                pauseGame();
            }
        }
    }

    public void pauseGame()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }
    public void resumeGame()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
    }

}