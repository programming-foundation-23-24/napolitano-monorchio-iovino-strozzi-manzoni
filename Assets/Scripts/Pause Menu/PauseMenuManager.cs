using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuManager : MonoBehaviour
{
    public GameObject pauseMenu; // Assicurati di assegnare il tuo menu di pausa nell'Inspector
    public bool isPaused;

    void Update()
    {
        isPaused = pauseMenu.activeSelf;
        // Verifica se il tasto Esc � premuto
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Attiva o disattiva il menu di pausa
            TogglePauseMenu();
        }
    }

    void TogglePauseMenu()
    {
        // Inverti lo stato attuale del menu di pausa
        pauseMenu.SetActive(!pauseMenu.activeSelf);

        // Non mettere in pausa o riprendere il tempo del gioco

        // Puoi inserire ulteriori azioni di pausa/riprendi qui, se necessario
    }
    public bool GetIsPaused()
    {
        return isPaused;
    }
}


