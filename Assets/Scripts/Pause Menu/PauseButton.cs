using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseButton : MonoBehaviour
{
    [SerializeField] GameObject option;
    [SerializeField] GameObject main;
    public static bool onClickMenu = false;

    public void MainMenu()
    {
        onClickMenu = true;
        SceneManager.LoadScene(0);
    }

    public void OpenOption()
    {
        option.SetActive(true);
        main.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
