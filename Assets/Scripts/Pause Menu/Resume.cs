using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResumeButton : MonoBehaviour
{
    public GameObject pauseMenu; // Assicurati di assegnare il tuo menu di pausa nell'Inspector

    public void OnResumeButtonClicked()
    {
        // Disattiva il menu di pausa
        pauseMenu.SetActive(false);

        // Riprendi il tempo del gioco
        Time.timeScale = 1f;

        // Puoi inserire ulteriori azioni qui, se necessario
    }
}



