using UnityEngine;

public class Passcode : MonoBehaviour
{
    public GameObject[] objectsArray; // Make sure to assign the objects in the Inspector
    public int currentPosition = 0;

    void OnEnable()
    {
        currentPosition = 0;
    }
    void Update()
    {
        // Deactivate all objects
        for (int i = 0; i < objectsArray.Length; i++)
        {
            objectsArray[i].SetActive(false);
        }

        // Activate only the object at the current position
        objectsArray[currentPosition].SetActive(true);
    }

    public void IncreasePosition()
    {
        currentPosition = (currentPosition + 1) % objectsArray.Length;
    }

    public void DecreasePosition()
    {
        currentPosition = (currentPosition - 1 + objectsArray.Length) % objectsArray.Length;
    }
}