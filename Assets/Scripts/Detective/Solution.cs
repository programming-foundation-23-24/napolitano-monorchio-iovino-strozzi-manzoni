using UnityEngine;

public class Solution : MonoBehaviour
{
    public int[] arrayToCheck = new int[4]; // Array of 4 integers to check
    public Passcode[] otherScript; // Reference to the script with the array
    public GameObject[] Buttons; // Make sure to assign the objects in the Inspector

    void Start()
    {
        Buttons[0].SetActive(true);
        Buttons[1].SetActive(false);
    }

    void Update()
    {
        // Check if the arrays match
        if (CheckArray())
        {
            Buttons[0].SetActive(false);
            Buttons[1].SetActive(true);
        }
        else 
        {
            Buttons[0].SetActive(true);
            Buttons[1].SetActive(false);
        }
    }

    public bool CheckArray()
    {
        // Check if the arrays match element by element
        for (int i = 0; i < arrayToCheck.Length; i++)
        {
            if (arrayToCheck[i] != otherScript[i].currentPosition)
            {
                return false; // Arrays do not match
            }
        }

        return true; // Arrays match
    }
}
