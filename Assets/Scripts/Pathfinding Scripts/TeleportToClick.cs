using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToClick : MonoBehaviour
{
    
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {

            Vector3 clickPosition = GetClickPosition();

            transform.position = new Vector3(clickPosition.x, clickPosition.y, 0f);
        }
    }

    Vector3 GetClickPosition()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
