using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAI.PathFinding;

public class RectGrid_Viz : MonoBehaviour
{
    // the max number of columns in the grid.
    public int mX;
    // the max number of rows in the grid
    public int mY;
    public bool onSecondInteraction;
    public GameObject inputRegister;
    // The prefab for representing a grid cell. We will 
    // use the prefab to show/visualize the status of the cell
    // as we proceed with our pathfinding.
    [SerializeField]
    GameObject RectGridCell_Prefab;

    GameObject[,] mRectGridCellGameObjects;

    // the 2d array of Vecto2Int.
    // This stucture stores the 2d indices of the grid cells.
    protected Vector2Int[,] mIndices;

    // the 2d array of the RectGridCell.
    protected RectGridCell[,] mRectGridCells;

    public Color COLOR_WALKABLE = new Color(42 / 255.0f, 99 / 255.0f, 164 / 255.0f, 1.0f);
    public Color COLOR_NON_WALKABLE = new Color(0.0f, 0.0f, 0.0f, 1.0f);
    public Color COLOR_CURRENT_NODE = new Color(0.5f, 0.4f, 0.1f, 1.0f);
    public Color COLOR_ADD_TO_OPEN_LIST = new Color(0.2f, 0.7f, 0.5f, 1.0f);
    public Color COLOR_ADD_TO_CLOSED_LIST = new Color(0.5f, 0.5f, 0.5f, 1.0f);

    public Transform mDestination;
    public NPCMovement mNPCMovement;
    //GPT qui imposto il quadrato giallo da inspector
  
    // Construct a grid with the max cols and rows.
    public void Construct(int numX, int numY) //era protected
    {
        mX = numX;
        mY = numY;

        mIndices = new Vector2Int[mX, mY];
        mRectGridCellGameObjects = new GameObject[mX, mY];
        mRectGridCells = new RectGridCell[mX, mY];

        // Get existing child cells and assign them to the array
        for (int i = 0; i < mX; ++i)
        {
            for (int j = 0; j < mY; ++j)
            {
                string cellName = "riga" + (i+1) + "colonna" + (j+1);
                Transform cellTransform = transform.Find(cellName);

                if (cellTransform != null)
                {
                    mIndices[i, j] = new Vector2Int(i, j);
                    mRectGridCellGameObjects[i, j] = cellTransform.gameObject;

                    // Create the RectGridCells
                    mRectGridCells[i, j] = new RectGridCell(this, mIndices[i, j]);

                    // Set a reference to the RectGridCell_Viz if available
                    RectGridCell_Viz rectGridCell_Viz = mRectGridCellGameObjects[i, j].GetComponent<RectGridCell_Viz>();
                    if (rectGridCell_Viz != null)
                    {
                        rectGridCell_Viz.RectGridCell = mRectGridCells[i, j];
                    }
                }
                else
                {
                    Debug.LogError("Cell not found: " + cellName);
                }
            }
        }
    }
    public int GetGridX()
    {
        return mX;
    }
    public int GetGridY()
    {
        return mY;
    }
    void ResetCamera()
    {
        Camera.main.orthographicSize = mY / 2.0f + 1.0f;
        Camera.main.transform.position = new Vector3(mX / 2.0f - 0.5f, mY / 2.0f - 2.0f, -100.0f);
    }

    private void Start()
    {
        // Constryct the grid and the cell game objects.
        Construct(mX, mY);

        // Reset the camera to a proper size and position.
        //ResetCamera();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(1))
        {
            Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            inputRegister.transform.position = new Vector3(clickPosition.x, clickPosition.y, 0f);
            RayCastAndSetDestination(inputRegister.transform.position);
        }
    }
    public List<Node<Vector2Int>> GetNeighbourCells(Node<Vector2Int> loc)
    {
        List<Node<Vector2Int>> neighbours = new List<Node<Vector2Int>>();

        int x = loc.Value.x;
        int y = loc.Value.y;

        // Check up.
        if (y < mY - 1)
        {
            int i = x;
            int j = y + 1;

            if (IsCellWalkable(i, j))
            {
                neighbours.Add(mRectGridCells[i, j]);
            }
        }
        // Check right
        if (x < mX - 1)
        {
            int i = x + 1;
            int j = y;

            if (IsCellWalkable(i, j))
            {
                neighbours.Add(mRectGridCells[i, j]);
            }
        }
        // Check down
        if (y > 0)
        {
            int i = x;
            int j = y - 1;

            if (IsCellWalkable(i, j))
            {
                neighbours.Add(mRectGridCells[i, j]);
            }
        }
        // Check left
        if (x > 0)
        {
            int i = x - 1;
            int j = y;

            if (IsCellWalkable(i, j))
            {
                neighbours.Add(mRectGridCells[i, j]);
            }
        }

        return neighbours;
    }
    public List<RectGridCell> GetNeighbourCellsWithCellParameter(RectGridCell cell)
    {
        List<RectGridCell> neighbours = new List<RectGridCell>();

        if (cell != null)
        {
            int x = cell.Value.x;
            int y = cell.Value.y;

            // Check up.
            if (y < mY - 1 && IsCellWalkable(x, y + 1))
            {
                neighbours.Add(mRectGridCells[x, y + 1]);
            }
            // Check right
            if (x < mX - 1 && IsCellWalkable(x + 1, y))
            {
                neighbours.Add(mRectGridCells[x + 1, y]);
            }
            // Check down
            if (y > 0 && IsCellWalkable(x, y - 1))
            {
                neighbours.Add(mRectGridCells[x, y - 1]);
            }
            // Check left
            if (x > 0 && IsCellWalkable(x - 1, y))
            {
                neighbours.Add(mRectGridCells[x - 1, y]);
            }
        }

        return neighbours;
    }


    public bool IsCellWalkable(int x, int y)
    {
        if (x >= 0 && x < mX && y >= 0 && y < mY)
        {
            GameObject cellObject = mRectGridCellGameObjects[x, y];
            if (cellObject != null)
            {
                // Aggiungi qui la tua condizione per verificare se la cella ha il tag "Wall"
                return mRectGridCells[x, y].IsWalkable && !cellObject.CompareTag("wall");
            }
        }

        return false;
    }
    public bool IsCellWalkable(RectGridCell cell) //overlapping del metodo di sopra @Nyapo 
    {
        if (cell != null)
        {
            int x = cell.Value.x;
            int y = cell.Value.y;

            if (x >= 0 && x < mX && y >= 0 && y < mY)
            {
                GameObject cellObject = mRectGridCellGameObjects[x, y];

                // Aggiungi qui la tua condizione per verificare se la cella ha il tag "Wall"
                return mRectGridCells[x, y].IsWalkable && !(cellObject != null && cellObject.CompareTag("wall"));
            }
        }

        return false;
    }


    void RayCastAndSetDestination(Vector2 rayPos)
    {
        
        RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, Mathf.Infinity);

        if (hit.collider != null && hit.transform.tag == "Untagged")
        {
            GameObject obj = hit.transform.gameObject;
            RectGridCell_Viz sc = obj.GetComponent<RectGridCell_Viz>();
            if (sc == null) return;

            Vector3 pos = mDestination.position;
            pos.x = sc.RectGridCell.Value.y;
            pos.y = sc.RectGridCell.Value.x;
            mDestination.position = pos;

            mNPCMovement.SetDestination(this, sc.RectGridCell);
        }
        else
        {
            mNPCMovement.TryNearbyCells();
        }
}

    public RectGridCell GetRectGridCell(int x, int y)
    {
        if(x >= 0 && x < mX && y >=0 && y < mY)
        {
            return mRectGridCells[x, y];
        }
        return null;
    }

    public static float GetManhattanCost(Vector2Int a,Vector2Int b)
    {
        return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y);
    }

    public static float GetEuclideanCost(Vector2Int a,Vector2Int b)
    {
        return GetCostBetweenTwoCells(a, b);
    }

    public static float GetCostBetweenTwoCells(Vector2Int a, Vector2Int b)
    {
        return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    public void ResetCellColours()
    {
       /* for(int i = 0; i < mX; i++)
        {
            for (int j = 0; j < mY; j++)
            {
                GameObject obj = mRectGridCellGameObjects[i, j];
                RectGridCell_Viz sc = obj.GetComponent<RectGridCell_Viz>();
                if(sc.RectGridCell.IsWalkable)
                {
                    sc.SetInnerColor(COLOR_WALKABLE);
                }
                else
                {
                    sc.SetInnerColor(COLOR_NON_WALKABLE);
                }
            }
        }*/
    }
    public RectGridCell GetSelectedCell()
    {
        //qui ho ricorretto di nuovo il punto di partenza del raycast disaccoppiandolo dal movimento del mouse utilizzando un gameObject esterno @Nyapo
        Vector2 rayPos = inputRegister.transform.position;
        RaycastHit2D hit = Physics2D.Raycast(rayPos, Vector2.zero, 0f);

        if (hit)
        {
            GameObject obj = hit.transform.gameObject;
            RectGridCell_Viz cellViz = obj.GetComponent<RectGridCell_Viz>();
            /* if (cellViz != null)
             {
                 return cellViz.RectGridCell;
             }*/
             if (cellViz == null)
            {
                
                cellViz = obj.GetComponent<RectGridCell_Viz>();

            }else
            {
                return cellViz.RectGridCell;
            }
        }
        return null;
        

    }
    public void OnChangeCurrentNode(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        GameObject obj = mRectGridCellGameObjects[x, y];
        RectGridCell_Viz sc = obj.GetComponent<RectGridCell_Viz>();
        sc.SetInnerColor(COLOR_CURRENT_NODE);
    }
    public void OnAddToOpenList(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        GameObject obj = mRectGridCellGameObjects[x, y];
        RectGridCell_Viz sc = obj.GetComponent<RectGridCell_Viz>();
        sc.SetInnerColor(COLOR_ADD_TO_OPEN_LIST);
    }
    public void OnAddToClosedList(PathFinder<Vector2Int>.PathFinderNode node)
    {
        int x = node.Location.Value.x;
        int y = node.Location.Value.y;
        GameObject obj = mRectGridCellGameObjects[x, y];
        RectGridCell_Viz sc = obj.GetComponent<RectGridCell_Viz>();
        sc.SetInnerColor(COLOR_ADD_TO_CLOSED_LIST);
    }
    //metodo che uso per fare debugging @Nyapo
    public void SetCellColorToYellow(RectGridCell selectedCell)
    {
        if (selectedCell != null)
        {
            int x = selectedCell.Value.x;
            int y = selectedCell.Value.y;

            if (x >= 0 && x < mX && y >= 0 && y < mY)
            {
                GameObject obj = mRectGridCellGameObjects[x, y];
                RectGridCell_Viz cellViz = obj.GetComponent<RectGridCell_Viz>();

                if (cellViz != null)
                {
                    cellViz.SetInnerColor(Color.yellow);
                }
            }
        }
    }
    public RectGridCell GetCellBelow(RectGridCell cell)
    {
        if (cell != null)
        {
            int x = cell.Value.x;
            int y = cell.Value.y;

            // Verifica se esiste una cella sotto la cella data
            if (y > 0)
            {
                return mRectGridCells[x, y - 1];
            }
        }

        // Se la cella data � nella prima riga o non � valida, restituisci null
        return null;
    }
    //metodo in perfezionamento ma possibilmente inutile, ci lavorer� a tempo perso @Nyapo
    public RectGridCell FindNearestWalkableCell(RectGridCell nonWalkableCell)
    {
        if (nonWalkableCell != null && !nonWalkableCell.IsWalkable)
        {
            Queue<RectGridCell> queue = new Queue<RectGridCell>();
            HashSet<RectGridCell> visited = new HashSet<RectGridCell>();

            queue.Enqueue(nonWalkableCell);
            visited.Add(nonWalkableCell);

            while (queue.Count > 0)
            {
                RectGridCell currentCell = queue.Dequeue();

                // Debug per visualizzare quale cella sta controllando
                Debug.Log("Checking cell: " + currentCell.Value);

                // Trovato! Restituisci la cella walkable pi� vicina.
                if (currentCell.IsWalkable)
                {
                    Debug.Log("Found walkable cell: " + currentCell.Value);
                    return currentCell;
                }

                // Aggiungi le celle vicine alla coda se non sono state visitate.
                foreach (var neighbor in GetNeighbourCellsWithCellParameter(currentCell))
                {
                    if (!visited.Contains(neighbor))
                    {
                        queue.Enqueue(neighbor);
                        visited.Add(neighbor);
                    }
                }
            }

            Debug.Log("No walkable cell found.");
        }
        else
        {
            Debug.LogError("Invalid or walkable cell provided.");
        }

        // Se non � stata trovata alcuna cella walkable, restituisci null.
        return null;
    }

}
