using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class RenameChildren : MonoBehaviour
{
    public int maxRows;
    public int maxColumns;

    public int riga = 1;
    public int colonna = 0;
    [Header("NON PREMETE QUESTO BOTTONE PER NESSUN MOTIVO, SERVE A ME E A TOMMASO")]
    public bool destroyChildrenOnRename = false; // da lasciare nascosto ai pericolosi designer 
    public bool startRenaming = false;
#if UNITY_EDITOR
    [ContextMenu("Rename Children")]
    void OnDrawGizmos()
    { 
        if (destroyChildrenOnRename)
        {
            DestroyAllChildren();
        }
        if (startRenaming)
        {
            Rename();

        }
    }


    public void Rename()
    {
        if (riga < 0 || riga >= maxRows || colonna < 0 || colonna >= maxColumns)
        {
            Debug.LogWarning("Valori di riga e/o colonna non validi.");
            return;
        }

        

        foreach (Transform child in transform)
        {
            Undo.RecordObject(child, "Rename Child");
            child.gameObject.name = "riga" + riga.ToString() + "colonna" + colonna.ToString();
            riga++;

            // Se abbiamo raggiunto il massimo numero di colonne, passa alla nuova riga
            if (riga > maxRows)
            {
                riga = 1; // Resetta il numero di colonna
                colonna++; // Passa alla nuova riga

                // Se abbiamo raggiunto il massimo numero di righe, interrompi
                if (colonna > maxColumns)
                {
                    colonna = 1;
                    Debug.LogWarning("Hai superato il numero massimo di righe.");
                    startRenaming = false;
                    return;
                }
            }
        }
    }

    public void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            Undo.DestroyObjectImmediate(child.gameObject);
        }

        if (transform.childCount  == 0)
	    {
            destroyChildrenOnRename = false;
	    }
    }
#endif
}
