using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorByTag : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Color baseColor;
    public Color wallColor;
    public Color pavementColor;
    public Color pavement2Color;

    new public GameObject gameObject;

    void OnDrawGizmos()
    {
        ApplyColors();
    }
    void Update()
    {
        ApplyColors();
    }

    void ApplyColors()
    {
        if (spriteRenderer != null)
        {
            CheckTagForColour(gameObject, spriteRenderer, "wall", wallColor);
            CheckTagForColour(gameObject, spriteRenderer, "pavement", pavementColor);
            CheckTagForColour(gameObject, spriteRenderer, "pavement2", pavement2Color);
            CheckTagForColour(gameObject, spriteRenderer, "Untagged", baseColor);
        }
    }

    private void CheckTagForColour(GameObject ob, SpriteRenderer colour, string tag, Color color)
    {
        // Check if the GameObject and SpriteRenderer references are not null
        if (ob != null && colour != null)
        {
            if (ob.tag == tag)
            {
                colour.color = color;
            }
        }
    }
}