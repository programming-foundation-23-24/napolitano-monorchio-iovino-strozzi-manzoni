using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAI.PathFinding;

public class RectGridCell_Viz : MonoBehaviour
{
  [SerializeField]
  SpriteRenderer InnerSprite;
  [SerializeField]
  SpriteRenderer OuterSprite;

  public RectGridCell RectGridCell;


  public void SetInnerColor(Color col)
  {
    InnerSprite.color = col;
  }

  public void SetOuterColor(Color col)
  {
    OuterSprite.color = col;
  }

 
}
