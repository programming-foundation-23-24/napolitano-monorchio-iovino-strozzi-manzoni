using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditCollider : MonoBehaviour
{

    public BoxCollider2D boxCollider;
    public bool changeColliderStatus;
    public Vector2 colliderSizeONE;
    public Vector2 colliderSizeTWO;
    public Vector2 colliderOffsetONE;
    public Vector2 colliderOffsetTWO;
    int i = 0;
    void Start()
    {
        boxCollider = gameObject.GetComponent<BoxCollider2D>();
    }
    private void Update()
    {
        if (changeColliderStatus)
        {
            boxCollider.size = colliderSizeONE;
            boxCollider.offset = colliderOffsetONE;

        }
        else
        {
            boxCollider.size = colliderSizeTWO;
            boxCollider.offset = colliderOffsetTWO;
        }
    }



}
