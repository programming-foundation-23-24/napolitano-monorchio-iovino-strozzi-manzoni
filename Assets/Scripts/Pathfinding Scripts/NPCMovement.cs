using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAI.PathFinding;

public class NPCMovement : MonoBehaviour
{
    public float Speed = 1.0f;
    public RectGrid_Viz grid;
    public Queue<Vector2> mWayPoints = new Queue<Vector2>();
    public PathFinder<Vector2Int> mPathFinder = new AStarPathFinder<Vector2Int>();
    public bool isRunning = false;

    void OnEnable()
    {
        
        StartCoroutine(Coroutine_MoveTo());
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }
    void Start()
    {

        if (grid == null)
        {
            Debug.LogError("RectGrid_Viz non trovato nella scena.");
        }
        SetPathFindingStatuses();

    }

    private void SetPathFindingStatuses()
    {
        mPathFinder.onSuccess = OnSuccessPathFinding;
        mPathFinder.onFailure = OnFailurePathFinding;
        mPathFinder.HeuristicCost = RectGrid_Viz.GetManhattanCost;
        mPathFinder.NodeTraversalCost = RectGrid_Viz.GetEuclideanCost;
    }

    public bool GetIsRunnig()
    {
        return isRunning;
    }

    public void AddWayPoint(Vector2 pt)
    {
        mWayPoints.Enqueue(pt);
    }
 
    public void SetDestination( RectGrid_Viz map,  RectGridCell destination)
    {

        if (mPathFinder.Status == PathFinderStatus.RUNNING)
        {
            mPathFinder.ForceReset();
            Debug.Log("Pathfinder already running. Cannot set destination now");
        }

        mWayPoints.Clear();

        RectGridCell start = map.GetRectGridCell((int)transform.position.y, (int)transform.position.x);
       
        if (start == null) return;

        map.ResetCellColours();

        mPathFinder.Initialize(start, destination);
        StartCoroutine(Coroutine_FindPathSteps());
    }

    IEnumerator Coroutine_FindPathSteps()
    {
        while(mPathFinder.Status == PathFinderStatus.RUNNING)
        {
            mPathFinder.Step();
            yield return null;
        }
    }

    void OnSuccessPathFinding()
    {
        PathFinder<Vector2Int>.PathFinderNode node = mPathFinder.CurrentNode;
        List<Vector2Int> reverse_indices = new List<Vector2Int>();
        while(node != null)
        {
            reverse_indices.Add(node.Location.Value);
            node = node.Parent;
        }
        for(int i = reverse_indices.Count -1; i >= 0; i--)
        {
            AddWayPoint(new Vector2(reverse_indices[i].y, reverse_indices[i].x));
        }
    }

    void OnFailurePathFinding()
    {
        TryNearbyCells();
    }

    public void TryNearbyCells()
    {
        RectGridCell selectedCell = grid.GetSelectedCell();
        List<RectGridCell> checkedCellsList = new List<RectGridCell>();

        if (TryAdjacentCells(selectedCell, 4, checkedCellsList))
        {
            Debug.Log("funziona");
        }
        else
        {
            
            ResetProcess();
        }
    }
    private RectGridCell lastCheckedCell;

    public bool TryAdjacentCells(RectGridCell currentCell, int maxAttempts, List<RectGridCell> checkedCellsList)
    {
        if (maxAttempts <= 0)
        {
            // Non ci sono pi� tentativi disponibili
            return false;
        }

        List<RectGridCell> neighboursCells = grid.GetNeighbourCellsWithCellParameter(currentCell);

        foreach (RectGridCell cell in neighboursCells)
        {
            grid.SetCellColorToYellow(cell);

            if (grid.IsCellWalkable(cell))
            {
                SetDestination(grid, cell);
                return true; // Cella percorribile trovata
            }
            else if (!checkedCellsList.Contains(cell))
            {
                checkedCellsList.Add(cell);
                lastCheckedCell = cell;

                // Chiamata ricorsiva per esplorare le celle adiacenti della cella attuale
                if (TryAdjacentCells(cell, maxAttempts - 1, checkedCellsList))
                {
                    return true; // Cella percorribile trovata nelle celle adiacenti
                }
            }
        }

        return false; // Nessuna cella percorribile trovata nelle celle adiacenti
    }

    public void ResetProcess()
    {
        if (lastCheckedCell != null)
        {
            List<RectGridCell> neighboursOfLastCheckedCell = grid.GetNeighbourCellsWithCellParameter(lastCheckedCell);

            foreach (RectGridCell neighbourCell in neighboursOfLastCheckedCell)
            {
                grid.SetCellColorToYellow(neighbourCell);

                if (grid.IsCellWalkable(neighbourCell))
                {
                    SetDestination(grid,neighbourCell);
                    return; // Termina se una cella percorribile � stata trovata
                }
            }
        }

        // Nessuna cella percorribile trovata nelle celle adiacenti delle celle precedentemente esaminate
        Debug.Log("Nessuna cella percorribile trovata nelle celle adiacenti delle celle precedentemente esaminate.");
    }

    public IEnumerator Coroutine_MoveTo()
    {
        
        while (true)
        {
            while (mWayPoints.Count > 0)
            {
                yield return StartCoroutine(Coroutine_MoveToPoint(mWayPoints.Dequeue(), Speed));
            }
            yield return null;
        }
    }

    private IEnumerator Coroutine_MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = 
            Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }
        objectToMove.transform.position = new Vector3(end.x, end.y, 0);
        isRunning = false;
    }

    IEnumerator Coroutine_MoveToPoint(Vector2 p, float speed)
    {
        isRunning = true;
        Vector3 endP = new Vector3(p.x, p.y, transform.position.z);
        float duration = (transform.position - endP).magnitude / speed;
        yield return StartCoroutine(Coroutine_MoveOverSeconds(transform.gameObject, endP, duration));
    }
}