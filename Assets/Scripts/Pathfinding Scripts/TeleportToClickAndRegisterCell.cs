using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportToClickAndRegisterCell : MonoBehaviour
{
    public NPCMovement nPCMovement;
    public bool isRunning;
    void Update()
    {
        isRunning = nPCMovement.GetIsRunnig();

        if (Input.GetMouseButtonDown(0) && !isRunning)
        {
            // Ottieni la posizione del clic del mouse in coordinate del mondo
            Vector3 clickPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Imposta la posizione del GameObject al punto in cui � stato cliccato
            transform.position = new Vector3(clickPosition.x, clickPosition.y, 0f);
        }
    }

}
