using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    [SerializeField] Slider musicVolumeSlider;
    [SerializeField] Slider effectsVolumeSlider;
    [SerializeField] AudioMixer mixer;

    void Start()
    {
        if (!PlayerPrefs.HasKey("musicVolume"))
        {
            PlayerPrefs.SetFloat("musicVolume", 1);
            PlayerPrefs.SetFloat("effectsVolume", 1);
            Load();
        }
        else
        {
            Load();
        }
    }

    public void ChangeMusicVolume()
    {
        float musicVolume = musicVolumeSlider.value;
        if (musicVolume > 0)
        {
            mixer.SetFloat("Music", Mathf.Log10(musicVolume) * 20);
        }
        else
        {
            mixer.SetFloat("Music", -80); // Imposta il volume su un valore basso (ad esempio, -80 dB) quando il volume � 0.
        }

        Save();
    }

    public void ChangeEffectsVolume()
    {
        float effectsVolume = effectsVolumeSlider.value;
        if (effectsVolume > 0)
        {
            mixer.SetFloat("SoundEffects", Mathf.Log10(effectsVolume) * 20);
        }
        else
        {
            mixer.SetFloat("SoundEffects", -80); // Imposta il volume su un valore basso (ad esempio, -80 dB) quando il volume � 0.
        }

        Save();
    }

    private void Load()
    {
        musicVolumeSlider.value = PlayerPrefs.GetFloat("musicVolume");
        effectsVolumeSlider.value = PlayerPrefs.GetFloat("effectsVolume");
    }

    private void Save()
    {
        PlayerPrefs.SetFloat("musicVolume", musicVolumeSlider.value);
        PlayerPrefs.SetFloat("effectsVolume", effectsVolumeSlider.value);
    }
}
