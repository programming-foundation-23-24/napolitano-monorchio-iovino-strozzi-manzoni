using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryMenu : MonoBehaviour
{
	[SerializeField] GameObject story;
	[SerializeField] GameObject main;
	
    public void Back()
    {

		main.SetActive(true);
		story.SetActive(false);

    }
}
