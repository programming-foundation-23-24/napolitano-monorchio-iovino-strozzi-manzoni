using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	[SerializeField] GameObject story;
	[SerializeField] GameObject main;
	[SerializeField] GameObject play;
	[SerializeField] GameObject option;

    public void Story()
    {
        story.SetActive(true);
        main.SetActive(false);
    }

    public void PlayLevels()
    {
        play.SetActive(true);
        main.SetActive(false);
    }

    public void OpenOption()
    {
        option.SetActive(true);
        main.SetActive(false);
    }
}
