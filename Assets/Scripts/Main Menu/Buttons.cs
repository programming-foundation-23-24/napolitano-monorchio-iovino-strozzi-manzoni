using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Buttons : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] GameObject onEnter;
    [SerializeField] GameObject onExit;

   

    public void OnEnable()
    {
        onExit.SetActive(true);
        onEnter.SetActive(false);
    }

    public void Start()
    {
        onExit.SetActive(true);
        onEnter.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    

        onExit.SetActive(false);
        onEnter.SetActive(true);

    }

    public void OnPointerExit(PointerEventData eventData)
    {
    

        onExit.SetActive(true);
        onEnter.SetActive(false);
    }
}
