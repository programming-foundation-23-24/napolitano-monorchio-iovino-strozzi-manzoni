using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScreen : MonoBehaviour
{
    [SerializeField] GameObject titleScreen;
    [SerializeField] GameObject background;
    [SerializeField] GameObject main;

    private bool anyKeyWasPressed = false;

    public void Start()
    {
        main.SetActive(false);
        background.SetActive(false);
    }

    public void Update()
    {
        if (titleScreen.activeSelf && !anyKeyWasPressed && Input.anyKeyDown)
        {
            anyKeyWasPressed = true;
            main.SetActive(true);
            titleScreen.SetActive(false);
            background.SetActive(true);
        }
        else if (!titleScreen.activeSelf)
        {
            anyKeyWasPressed = false;
        }
    }
}
