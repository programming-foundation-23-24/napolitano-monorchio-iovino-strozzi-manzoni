using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMenu : MonoBehaviour
{
	[SerializeField] GameObject play;
	[SerializeField] GameObject main;

    public void Level0()
    {
        SceneManager.LoadScene(0);
    }

    public void Level1()
    {
	SceneManager.LoadScene(1);
    }

    public void Level2()
    {
	SceneManager.LoadScene(2);
    }

    public void Level3()
    {
	SceneManager.LoadScene(3);
    }

    public void Level4()
    {
	SceneManager.LoadScene(4);
    }

    public void Back()
    {
		main.SetActive(true);
		play.SetActive(false);
    }
	
}
