using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThugTeleport : MonoBehaviour
{
    public Transform cellRaycastChange;
    public Transform thug;

    public void FadeEvent()
    {
        thug.gameObject.GetComponent<NPCEventActivation>().cellRaycast = cellRaycastChange.GetComponent<OpenActionMenu>();
        thug.position = new Vector3(cellRaycastChange.position.x, cellRaycastChange.position.y, thug.position.z);
    }

    public void Update()
    {
        var npcEventActivation = thug.gameObject.GetComponent<NPCEventActivation>();
        if (npcEventActivation != null && npcEventActivation.cellRaycast == cellRaycastChange.GetComponent<OpenActionMenu>())
        {
            var turnOnDescription = cellRaycastChange.GetComponentInChildren<TurnOnDescription>(true);
            if (turnOnDescription != null)
            {
                turnOnDescription.enabled = true;
            }

            var npcGameObject = cellRaycastChange.Find("NPC")?.gameObject;
            if (npcGameObject != null)
            {
                npcGameObject.SetActive(true);
            }

            cellRaycastChange.gameObject.tag = "wall";
        }
        else
        {
            var turnOnDescription = cellRaycastChange.GetComponentInChildren<TurnOnDescription>(true);
            if (turnOnDescription != null)
            {
                turnOnDescription.enabled = false;
            }

            var npcGameObject = cellRaycastChange.Find("NPC")?.gameObject;
            if (npcGameObject != null)
            {
                npcGameObject.SetActive(false);
            }

            cellRaycastChange.gameObject.tag = "Untagged";
        }
    }

}